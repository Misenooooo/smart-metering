package bakalarska_praca_xdolnak

class UrlMappings {

    static mappings = {


        "/"(controller: 'frontend', action: "homepage")

        "/customerDataPortRest/monthlyData"(controller: 'customerDataPortRest', action: 'monthlyData')

        "/customerProfileRest/getAllDistributors"(controller: 'customerProfileRest', action: 'getAllDistributors')

        "/household"(controller: 'customer', action: "index")

        "/appliances"(controller: 'customer', action: "appliances")

        "/calculator"(controller: 'customer', action: "calculator")

        "/rooms"(controller: 'customer', action: 'rooms')

        "/profile"(controller: 'customer', action: 'profile')

        "/comparison"(controller: 'customer', action: 'comparison')

        "/login"(controller: 'login', action: 'auth')

        "/login/denied"(controller: 'frontend', action: "error")

        "500"(view:'/error')

        "404"(view:'/notFound')


    }
}
