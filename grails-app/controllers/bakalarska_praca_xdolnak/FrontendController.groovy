package bakalarska_praca_xdolnak

import grails.plugin.springsecurity.annotation.Secured

@Secured(["permitAll"])
class FrontendController {

    def homepage() {
        render view: 'homepage'
    }

    def features() {

    }

}
