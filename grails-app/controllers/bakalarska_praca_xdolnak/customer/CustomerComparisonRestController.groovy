package bakalarska_praca_xdolnak.customer

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(["ROLE_CUSTOMER"])
class CustomerComparisonRestController {

    def dataportVisualizationService

    def customerDAOService

    def authentificationService

    def getAllBranchesWithData(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAllAppliancesOrRooms(accommodationUnit)
        render response as JSON
    }

    def getAverageForSelectedBranch(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAverageForSelectedBranch(accommodationUnit, request.JSON.branch)
        render response as JSON
    }

    def getAllConsumptionDataForBranch(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAllConsumptionForApplianceByDay(accommodationUnit,request.JSON.branch)
        render response as JSON
    }

    def getComparisonDataForBranches(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getComparisonDataForBranches(accommodationUnit)
        render response as JSON
    }

}
