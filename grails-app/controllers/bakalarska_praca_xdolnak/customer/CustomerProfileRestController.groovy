package bakalarska_praca_xdolnak.customer

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.http.HttpStatus
import sk.smartMetring.customer.Tariff

@Secured(["ROLE_CUSTOMER"])
class CustomerProfileRestController {

    def customerDAOService

    def authentificationService

    def getAllDistributors(){
        def response = customerDAOService.getAllDistributors()
        render response as JSON
    }

    def getAllTariffsForDistributor(){
        def response = customerDAOService.getAllTariffsForDistributor(request.JSON.selectedDistributor)
        render response as JSON
    }

    def getSelectedDistributor(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = accommodationUnit.tariff.electricityDistributor
        render response as JSON
    }

    def getSelectedTariff(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = accommodationUnit.tariff
        render response as JSON
    }

}
