package bakalarska_praca_xdolnak.customer

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(["ROLE_CUSTOMER"])
class CustomerDataPortRestController {

    def authentificationService

    def dataportVisualizationService

    def getAllConsumptionDataForCalendar(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAllConsumptionDataByDay(accommodationUnit)
        render response as JSON
    }

    def getAllDailyConsumptionData(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getConsumptionDataForDay(accommodationUnit,request.JSON.date)
        render response as JSON
    }

    def getAllAppliances(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAllAppliances(accommodationUnit)
        render response as JSON
    }

    def getAllRooms(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAllRooms(accommodationUnit)
        render response as JSON
    }

    def getAllConsumptionDataForAppliance(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAllConsumptionForApplianceByDay(accommodationUnit,request.JSON.selectedAppliance)
        render response as JSON
    }

    def getAllDailyConsumptionDataForAppliance(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportVisualizationService.getAllConsumptionForApplianceForDay(accommodationUnit,request.JSON.applianceName,request.JSON.date)
        render response as JSON
    }

}
