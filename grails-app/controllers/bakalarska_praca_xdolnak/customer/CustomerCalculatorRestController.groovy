package bakalarska_praca_xdolnak.customer

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(["ROLE_CUSTOMER"])
class CustomerCalculatorRestController {

    def authentificationService

    def dataportDAOService

    def customerDAOService

    def getAllAppliancesForSelectedType(){
        def response = customerDAOService.getAllAppliancesOffers(request.JSON.selectedApplianceType)
        render response as JSON
    }

    def getAllConsumptionDataWithPriceForAppliance(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def response = dataportDAOService.getPriceForApplianceByDay(accommodationUnit, request.JSON.applianceName)
        render response as JSON
    }
}
