package bakalarska_praca_xdolnak.customer

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import sk.smartMetring.customer.AccommodationUnit
import sk.smartMetring.customer.CustomerAccount
import sk.smartMetring.customer.Tariff
import sk.smartMetring.security.Role
import sk.smartMetring.security.User
import sk.smartMetring.security.UserRole

@Secured(['ROLE_CUSTOMER'])
class CustomerController {

    def authentificationService

    def springSecurityService

    def index() {
        if (!authentificationService.accomodationUnitForUser){
            return redirect(controller:'customer', action:'pairing')
        }
    }

    def appliances() {
        if (!authentificationService.accomodationUnitForUser){
            return redirect(controller:'customer', action:'pairing')
        }
        render view: '../customer/appliances'
    }

    def rooms() {
        if (!authentificationService.accomodationUnitForUser){
            return redirect(controller:'customer', action:'pairing')
        }
        render view: '../customer/rooms'
    }

    def calculator() {
        if (!authentificationService.accomodationUnitForUser){
            return redirect(controller:'customer', action:'pairing')
        }
        render view: '../customer/calculator'
    }

    def profile(){
        if (!authentificationService.accomodationUnitForUser){
            return redirect(controller:'customer', action:'pairing')
        }
        def user = springSecurityService.getCurrentUser()
        render(view: '../customer/profile', model:[user:user])
    }

    def updateUser(){
        User oldUser = springSecurityService.getCurrentUser()
        if (User.findByUsername(params.username) && User.findByUsername(params.username) != oldUser) {
            return render(view: "profile", model: [user: oldUser, email: message(code: 'user.email', args: [params.username])])
        }
        try {
            oldUser.username = params.username
            oldUser.firstName = params.firstName
            oldUser.lastName = params.lastName
            oldUser.save(failOnError: true, flush:true)
        } catch (ValidationException e) {
            log.warn e, e
            flash.errors = e.errors
        }  catch (e) {
            log.error e, e
            flash.error = message(code: 'error.default.message')
        }
        render(view: "profile", model: [user: oldUser, success: message(code: 'customer.profile.successfully.updated')])
    }

    def updateAccommodationUnit(){
        def accommodationUnit  = authentificationService.accomodationUnitForUser
        def newTariff = Tariff.findById(request.JSON.selectedTariff)
        accommodationUnit.tariff = newTariff
        accommodationUnit.save(failOnError:true,flush:true)
        render accommodationUnit as JSON
    }


    def comparison(){
        if (!authentificationService.accomodationUnitForUser){
            return redirect(controller:'customer', action:'pairing')
        }
        render view: '../customer/comparison'
    }

    def pairing(){
        if (authentificationService.accomodationUnitForUser) {
            return redirect(controller: 'customer', action: 'index')
        }
        render view: '../customer/pairing'
    }

    def pairAccounts(){
        AccommodationUnit accommodationUnit = AccommodationUnit.findByHashCode(params.code)
        if (!accommodationUnit){
            flash.message = message(code: 'customer.pairing.wrongCode')
            return render(view:'pairing', model: ['code':params.code] )
        }
        def user = springSecurityService.getCurrentUser()
        def customerAccount = CustomerAccount.findByUser(user)
        accommodationUnit.addToCustomerAccounts(customerAccount)
        accommodationUnit.save(failOnError: true, flush:true)
        redirect(controller: 'customer', action: 'index')
    }
}
