package bakalarska_praca_xdolnak

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import sk.smartMetring.customer.CustomerAccount
import sk.smartMetring.security.Role
import sk.smartMetring.security.User
import sk.smartMetring.security.UserRole

@Secured(['permitAll'])
class RegistrationController {

    def springSecurityService

    def registration() {
        if (springSecurityService.loggedIn) {
            flash.message = message(code: 'user.registration.logout.first')
            return redirect(uri: "/")
        }
        [user: new User(params)]
    }


    def register(User user) {

        user.username = params.email
        user.validate()

        if(params.password != params.passwordRepeat) user.errors.rejectValue("password", "user.password.repeat.mismatch")
        if (User.findByUsername(params.email)) {
            return render(view: "registration", model: [user: user, messages: [errors: user.errors], email: message(code: 'user.emailIsNotUnique', args: [params.email])])
        }
        if (user.hasErrors()) return render(view: "registration", model: [user: user, messages: [errors: user.errors]])
        try {
            user.save(failOnError: true)
            def role = Role.findByAuthority(Role.CUSTOMER)
            if (!UserRole.findByUserAndRole(user, role)) {
                UserRole.create(user, role)
            }
            CustomerAccount customerAccount = new CustomerAccount(user: user).save(failOnError:true, flush: true)

            flash.message = message(code: 'registration.successfullyRegistered')
            return redirect(controller: "login", action: "auth")
        } catch (ValidationException e) {
            log.warn e, e
            flash.errors = e.errors
        }  catch (e) {
            log.error e, e
            flash.error = message(code: 'error.default.message')
        }
        redirect(view: "registration", params: params)
    }
}
