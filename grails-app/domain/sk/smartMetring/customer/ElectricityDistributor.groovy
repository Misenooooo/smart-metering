package sk.smartMetring.customer

class ElectricityDistributor {

    String name

    static constraints = {
        name nullable: false
    }
}
