package sk.smartMetring.customer

import sk.smartMetring.security.User

class CustomerAccount {

    User user

    static constraints = {
        user nullable: false, unique: true
    }
}
