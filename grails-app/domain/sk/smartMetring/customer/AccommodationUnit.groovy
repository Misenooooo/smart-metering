package sk.smartMetring.customer

/*Wrapper class over dataport data with some metadata*/
class AccommodationUnit {

    String dataportDataId

    String hashCode

    Tariff tariff

    static hasMany=[
           customerAccounts:CustomerAccount
    ]

    static constraints = {
        tariff nullable: true
        hashCode unique: true
        dataportDataId nullable: false, unique: true
    }
}
