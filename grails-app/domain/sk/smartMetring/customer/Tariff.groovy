package sk.smartMetring.customer

class Tariff {

    String name

    String code

    BigDecimal lowRate

    BigDecimal highRate

    ElectricityDistributor electricityDistributor

    static constraints = {
        name nullable: false
        code nullable: false, unique: true
        lowRate nullable: true, scale: 10
        highRate nullable: false, scale: 10
        electricityDistributor nullable: false
    }
}
