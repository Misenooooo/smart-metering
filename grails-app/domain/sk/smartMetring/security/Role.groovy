package sk.smartMetring.security

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class Role implements Serializable  {

	private static final long serialVersionUID = 1

	static final String ADMIN = 'ROLE_ADMIN'

	static final String CUSTOMER = 'ROLE_CUSTOMER'

	String authority

	static constraints = {
		authority blank: false, unique: true
	}

	static mapping = {
		cache true
	}
}
