package sk.smartMetring

class DailyData {

    Integer id
    String dataid
    String local15min
    BigDecimal used
    BigDecimal air1
    BigDecimal air2
    BigDecimal air3
    BigDecimal airwindowunit1
    BigDecimal aquarium1
    BigDecimal bathroom1
    BigDecimal bathroom2
    BigDecimal bedroom1
    BigDecimal bedroom2
    BigDecimal bedroom3
    BigDecimal bedroom4
    BigDecimal bedroom5
    BigDecimal car1
    BigDecimal clotheswasher1
    BigDecimal clotheswasherDryg1
    BigDecimal diningroom1
    BigDecimal diningroom2
    BigDecimal dishwasher1
    BigDecimal disposal1
    BigDecimal drye1
    BigDecimal dryg1
    BigDecimal freezer1
    BigDecimal furnace1
    BigDecimal furnace2
    BigDecimal garage1
    BigDecimal garage2
    BigDecimal gen
    BigDecimal grid
    BigDecimal heater1
    BigDecimal housefan1
    BigDecimal icemaker1
    BigDecimal jacuzzi1
    BigDecimal kitchen1
    BigDecimal kitchen2
    BigDecimal kitchenapp1
    BigDecimal kitchenapp2
    BigDecimal lightsPlugs1
    BigDecimal lightsPlugs2
    BigDecimal lightsPlugs3
    BigDecimal lightsPlugs4
    BigDecimal lightsPlugs5
    BigDecimal lightsPlugs6
    BigDecimal livingroom1
    BigDecimal livingroom2
    BigDecimal microwave1
    BigDecimal office1
    BigDecimal outsidelightsPlugs1
    BigDecimal outsidelightsPlugs2
    BigDecimal oven1
    BigDecimal oven2
    BigDecimal pool1
    BigDecimal pool2
    BigDecimal poollight1
    BigDecimal poolpump1
    BigDecimal pump1
    BigDecimal range1
    BigDecimal refrigerator1
    BigDecimal refrigerator2
    BigDecimal security1
    BigDecimal shed1
    BigDecimal sprinkler1
    BigDecimal utilityroom1
    BigDecimal venthood1
    BigDecimal waterheater1
    BigDecimal waterheater2
    BigDecimal winecooler1
    BigDecimal totalConsumption



    static mapping = {
        local15min column: '`local_15min`'
        dataid column: '`data_id`'

    }

    static constraints = {
        dataid nullable: false
        local15min nullable: false
        used nullable: true
        air1 nullable: true
        air2 nullable: true
        air3 nullable: true
        airwindowunit1 nullable: true
        aquarium1 nullable: true
        bathroom1 nullable: true
        bathroom2 nullable: true
        bedroom1 nullable: true
        bedroom2 nullable: true
        bedroom3 nullable: true
        bedroom4 nullable: true
        bedroom5 nullable: true
        car1 nullable: true
        clotheswasher1 nullable: true
        clotheswasherDryg1 nullable: true
        diningroom1 nullable: true
        diningroom2 nullable: true
        dishwasher1 nullable: true
        disposal1 nullable: true
        drye1 nullable: true
        dryg1 nullable: true
        freezer1 nullable: true
        furnace1 nullable: true
        furnace2 nullable: true
        garage1 nullable: true
        garage2 nullable: true
        gen nullable: true
        grid nullable: true
        heater1 nullable: true
        housefan1 nullable: true
        icemaker1 nullable: true
        jacuzzi1 nullable: true
        kitchen1 nullable: true
        kitchen2 nullable: true
        kitchenapp1 nullable: true
        kitchenapp2 nullable: true
        lightsPlugs1 nullable: true
        lightsPlugs2 nullable: true
        lightsPlugs3 nullable: true
        lightsPlugs4 nullable: true
        lightsPlugs5 nullable: true
        lightsPlugs6 nullable: true
        livingroom1 nullable: true
        livingroom2 nullable: true
        microwave1 nullable: true
        office1 nullable: true
        outsidelightsPlugs1 nullable: true
        outsidelightsPlugs2 nullable: true
        oven1 nullable: true
        oven2 nullable: true
        pool1 nullable: true
        pool2 nullable: true
        poollight1 nullable: true
        poolpump1 nullable: true
        pump1 nullable: true
        range1 nullable: true
        refrigerator1 nullable: true
        refrigerator2 nullable: true
        security1 nullable: true
        shed1 nullable: true
        sprinkler1 nullable: true
        utilityroom1 nullable: true
        venthood1 nullable: true
        waterheater1 nullable: true
        waterheater2 nullable: true
        winecooler1 nullable: true
        id generator:'identity', column:'id', name: 'id'
    }
}
