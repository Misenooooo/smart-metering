package sk.smartMetring

class Appliance {

    String name;

    String applianceType;

    BigDecimal yearlyConsumption;

    BigDecimal price;

    String webUrl;

    String imagePath;

    /*Parameters*/

    Integer capacity;


    static constraints = {
        applianceType nullable:false
        yearlyConsumption nullable:false, scale: 10
        price nullable:false, scale: 10
        capacity nullable:false
        webUrl nullable:false, unique: true
        imagePath nullable: false

        capacity nullable: true
    }
}
