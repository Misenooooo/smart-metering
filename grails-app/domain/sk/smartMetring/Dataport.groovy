package sk.smartMetring

import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder

class Dataport{

	Integer id
	String dataid
	String local15min
	String used
	String air1
	String air2
	String air3
	String airwindowunit1
	String aquarium1
	String bathroom1
	String bathroom2
	String bedroom1
	String bedroom2
	String bedroom3
	String bedroom4
	String bedroom5
	String car1
	String clotheswasher1
	String clotheswasherDryg1
	String diningroom1
	String diningroom2
	String dishwasher1
	String disposal1
	String drye1
	String dryg1
	String freezer1
	String furnace1
	String furnace2
	String garage1
	String garage2
	String gen
	String grid
	String heater1
	String housefan1
	String icemaker1
	String jacuzzi1
	String kitchen1
	String kitchen2
	String kitchenapp1
	String kitchenapp2
	String lightsPlugs1
	String lightsPlugs2
	String lightsPlugs3
	String lightsPlugs4
	String lightsPlugs5
	String lightsPlugs6
	String livingroom1
	String livingroom2
	String microwave1
	String office1
	String outsidelightsPlugs1
	String outsidelightsPlugs2
	String oven1
	String oven2
	String pool1
	String pool2
	String poollight1
	String poolpump1
	String pump1
	String range1
	String refrigerator1
	String refrigerator2
	String security1
	String shed1
	String sprinkler1
	String utilityroom1
	String venthood1
	String waterheater1
	String waterheater2
	String winecooler1

	int hashCode() {
		def builder = new HashCodeBuilder()
		builder.append dataid
		builder.append local15min
		builder.append used
		builder.append air1
		builder.append air2
		builder.append air3
		builder.append airwindowunit1
		builder.append aquarium1
		builder.append bathroom1
		builder.append bathroom2
		builder.append bedroom1
		builder.append bedroom2
		builder.append bedroom3
		builder.append bedroom4
		builder.append bedroom5
		builder.append car1
		builder.append clotheswasher1
		builder.append clotheswasherDryg1
		builder.append diningroom1
		builder.append diningroom2
		builder.append dishwasher1
		builder.append disposal1
		builder.append drye1
		builder.append dryg1
		builder.append freezer1
		builder.append furnace1
		builder.append furnace2
		builder.append garage1
		builder.append garage2
		builder.append gen
		builder.append grid
		builder.append heater1
		builder.append housefan1
		builder.append icemaker1
		builder.append jacuzzi1
		builder.append kitchen1
		builder.append kitchen2
		builder.append kitchenapp1
		builder.append kitchenapp2
		builder.append lightsPlugs1
		builder.append lightsPlugs2
		builder.append lightsPlugs3
		builder.append lightsPlugs4
		builder.append lightsPlugs5
		builder.append lightsPlugs6
		builder.append livingroom1
		builder.append livingroom2
		builder.append microwave1
		builder.append office1
		builder.append outsidelightsPlugs1
		builder.append outsidelightsPlugs2
		builder.append oven1
		builder.append oven2
		builder.append pool1
		builder.append pool2
		builder.append poollight1
		builder.append poolpump1
		builder.append pump1
		builder.append range1
		builder.append refrigerator1
		builder.append refrigerator2
		builder.append security1
		builder.append shed1
		builder.append sprinkler1
		builder.append utilityroom1
		builder.append venthood1
		builder.append waterheater1
		builder.append waterheater2
		builder.append winecooler1
		builder.toHashCode()
	}

	boolean equals(other) {
		if (other == null) return false
		def builder = new EqualsBuilder()
		builder.append dataid, other.dataid
		builder.append local15min, other.local15min
		builder.append used, other.used
		builder.append air1, other.air1
		builder.append air2, other.air2
		builder.append air3, other.air3
		builder.append airwindowunit1, other.airwindowunit1
		builder.append aquarium1, other.aquarium1
		builder.append bathroom1, other.bathroom1
		builder.append bathroom2, other.bathroom2
		builder.append bedroom1, other.bedroom1
		builder.append bedroom2, other.bedroom2
		builder.append bedroom3, other.bedroom3
		builder.append bedroom4, other.bedroom4
		builder.append bedroom5, other.bedroom5
		builder.append car1, other.car1
		builder.append clotheswasher1, other.clotheswasher1
		builder.append clotheswasherDryg1, other.clotheswasherDryg1
		builder.append diningroom1, other.diningroom1
		builder.append diningroom2, other.diningroom2
		builder.append dishwasher1, other.dishwasher1
		builder.append disposal1, other.disposal1
		builder.append drye1, other.drye1
		builder.append dryg1, other.dryg1
		builder.append freezer1, other.freezer1
		builder.append furnace1, other.furnace1
		builder.append furnace2, other.furnace2
		builder.append garage1, other.garage1
		builder.append garage2, other.garage2
		builder.append gen, other.gen
		builder.append grid, other.grid
		builder.append heater1, other.heater1
		builder.append housefan1, other.housefan1
		builder.append icemaker1, other.icemaker1
		builder.append jacuzzi1, other.jacuzzi1
		builder.append kitchen1, other.kitchen1
		builder.append kitchen2, other.kitchen2
		builder.append kitchenapp1, other.kitchenapp1
		builder.append kitchenapp2, other.kitchenapp2
		builder.append lightsPlugs1, other.lightsPlugs1
		builder.append lightsPlugs2, other.lightsPlugs2
		builder.append lightsPlugs3, other.lightsPlugs3
		builder.append lightsPlugs4, other.lightsPlugs4
		builder.append lightsPlugs5, other.lightsPlugs5
		builder.append lightsPlugs6, other.lightsPlugs6
		builder.append livingroom1, other.livingroom1
		builder.append livingroom2, other.livingroom2
		builder.append microwave1, other.microwave1
		builder.append office1, other.office1
		builder.append outsidelightsPlugs1, other.outsidelightsPlugs1
		builder.append outsidelightsPlugs2, other.outsidelightsPlugs2
		builder.append oven1, other.oven1
		builder.append oven2, other.oven2
		builder.append pool1, other.pool1
		builder.append pool2, other.pool2
		builder.append poollight1, other.poollight1
		builder.append poolpump1, other.poolpump1
		builder.append pump1, other.pump1
		builder.append range1, other.range1
		builder.append refrigerator1, other.refrigerator1
		builder.append refrigerator2, other.refrigerator2
		builder.append security1, other.security1
		builder.append shed1, other.shed1
		builder.append sprinkler1, other.sprinkler1
		builder.append utilityroom1, other.utilityroom1
		builder.append venthood1, other.venthood1
		builder.append waterheater1, other.waterheater1
		builder.append waterheater2, other.waterheater2
		builder.append winecooler1, other.winecooler1
		builder.isEquals()
	}

	static mapping = {
		local15min column: '`local_15min`'
	}

	static constraints = {
		id generator:'identity', column:'id', name: 'id'
		dataid nullable: true, type: 'text'
		local15min nullable: true, type: 'text'
		used nullable: true, type: 'text'
		air1 nullable: true, type: 'text'
		air2 nullable: true, type: 'text'
		air3 nullable: true, type: 'text'
		airwindowunit1 nullable: true, type: 'text'
		aquarium1 nullable: true, type: 'text'
		bathroom1 nullable: true, type: 'text'
		bathroom2 nullable: true, type: 'text'
		bedroom1 nullable: true, type: 'text'
		bedroom2 nullable: true, type: 'text'
		bedroom3 nullable: true, type: 'text'
		bedroom4 nullable: true, type: 'text'
		bedroom5 nullable: true, type: 'text'
		car1 nullable: true, type: 'text'
		clotheswasher1 nullable: true, type: 'text'
		clotheswasherDryg1 nullable: true, type: 'text'
		diningroom1 nullable: true, type: 'text'
		diningroom2 nullable: true, type: 'text'
		dishwasher1 nullable: true, type: 'text'
		disposal1 nullable: true, type: 'text'
		drye1 nullable: true, type: 'text'
		dryg1 nullable: true, type: 'text'
		freezer1 nullable: true, type: 'text'
		furnace1 nullable: true, type: 'text'
		furnace2 nullable: true, type: 'text'
		garage1 nullable: true, type: 'text'
		garage2 nullable: true, type: 'text'
		gen nullable: true, type: 'text'
		grid nullable: true, type: 'text'
		heater1 nullable: true, type: 'text'
		housefan1 nullable: true, type: 'text'
		icemaker1 nullable: true, type: 'text'
		jacuzzi1 nullable: true, type: 'text'
		kitchen1 nullable: true, type: 'text'
		kitchen2 nullable: true, type: 'text'
		kitchenapp1 nullable: true, type: 'text'
		kitchenapp2 nullable: true, type: 'text'
		lightsPlugs1 nullable: true, type: 'text'
		lightsPlugs2 nullable: true, type: 'text'
		lightsPlugs3 nullable: true, type: 'text'
		lightsPlugs4 nullable: true, type: 'text'
		lightsPlugs5 nullable: true, type: 'text'
		lightsPlugs6 nullable: true, type: 'text'
		livingroom1 nullable: true, type: 'text'
		livingroom2 nullable: true, type: 'text'
		microwave1 nullable: true, type: 'text'
		office1 nullable: true, type: 'text'
		outsidelightsPlugs1 nullable: true, type: 'text'
		outsidelightsPlugs2 nullable: true, type: 'text'
		oven1 nullable: true, type: 'text'
		oven2 nullable: true, type: 'text'
		pool1 nullable: true, type: 'text'
		pool2 nullable: true, type: 'text'
		poollight1 nullable: true, type: 'text'
		poolpump1 nullable: true, type: 'text'
		pump1 nullable: true, type: 'text'
		range1 nullable: true, type: 'text'
		refrigerator1 nullable: true, type: 'text'
		refrigerator2 nullable: true, type: 'text'
		security1 nullable: true, type: 'text'
		shed1 nullable: true, type: 'text'
		sprinkler1 nullable: true, type: 'text'
		utilityroom1 nullable: true, type: 'text'
		venthood1 nullable: true, type: 'text'
		waterheater1 nullable: true, type: 'text'
		waterheater2 nullable: true, type: 'text'
		winecooler1 nullable: true, type: 'text'
	}
}
