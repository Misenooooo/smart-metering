
app{
    name = "smart-metring"

	lang.allowed = ['en', 'sk']

	context = '/'
}

grails.web.url.converter = 'hyphenated'

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.auth.loginFormUrl = '/login' // '/'
grails.plugin.springsecurity.logout.filterProcessesUrl = '/logout/index'

grails.plugin.springsecurity.apf.filterProcessesUrl = '/security/login_acccess_check'    //Login form post URL, intercepted by Spring Security filter.
grails.plugin.springsecurity.apf.usernameParameter = 'security_username'  //	Login form username parameter.
grails.plugin.springsecurity.apf.passwordParameter = 'security_password'  //	Login form password parameter.

grails.plugin.springsecurity.userLookup.userDomainClassName = 'sk.smartMetring.security.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'sk.smartMetring.security.UserRole'
grails.plugin.springsecurity.authority.className = 'sk.smartMetring.security.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
		[pattern: '/',               access: ['permitAll']],
		[pattern: '/error',          access: ['permitAll']],
		[pattern: '/index',          access: ['permitAll']],
		[pattern: '/index.gsp',      access: ['permitAll']],
		[pattern: '/shutdown',       access: ['permitAll']],
		[pattern: '/assets/**',      access: ['permitAll']],
		[pattern: '/**/js/**',       access: ['permitAll']],
		[pattern: '/**/css/**',      access: ['permitAll']],
		[pattern: '/**/images/**',   access: ['permitAll']],
		[pattern: '/**/fonts/**',    access: ['permitAll']],
		[pattern: '/**/favicon.ico', access: ['permitAll']],
		[pattern: '/check', 		 access: ['permitAll']],
		[pattern: '/**', 	  		 access: ['ROLE_ADMIN']],
]

grails.plugin.springsecurity.filterChain.chainMap = [
		[pattern: '/assets/**',      filters: 'none'],
		[pattern: '/**/js/**',       filters: 'none'],
		[pattern: '/**/css/**',      filters: 'none'],
		[pattern: '/**/images/**',   filters: 'none'],
		[pattern: '/**/fonts/**',    filters: 'none'],
		[pattern: '/**/favicon.ico', filters: 'none'],
		[pattern: '/**',             filters: 'JOINED_FILTERS'],
]

grails.plugin.springsecurity.rejectIfNoRule = true
grails.plugin.springsecurity.fii.rejectPublicInvocations = true
grails.plugin.springsecurity.securityConfigType = "Annotation"
grails.plugin.springsecurity.adh.errorPage = '/login/denied'
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/household'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.password.algorithm = 'SHA-256'
grails.plugin.springsecurity.password.hash.iterations = 1
grails.plugin.springsecurity.useSecurityEventListener = true
grails.plugin.springsecurity.useSwitchUserFilter = true

def homeDirectory = ""
environments {
	development {
		homeDirectory = System.getProperty("user.home") + "/smart-metring/"
	}
}
upload.directory.appliance = homeDirectory + "appliances"


grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*', '/fonts/*']
grails.resources.adhoc.includes = ['/images/**', '/css/**', '/js/**', '/plugins/**', '/fonts/**']
