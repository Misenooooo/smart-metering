package data

import org.springframework.context.i18n.LocaleContextHolder

class SmartmetringTagLib {
    static namespace = "bp"
    static returnObjectForTags = ['makeAbsoluteLink']

    def localeService
    def messageSource

    def flagSelectorLandingPage = { attrs ->
        def locale = LocaleContextHolder.locale
        def msg
        try {
            msg = messageSource.getMessage("all.locale.$locale.language", null, LocaleContextHolder.locale)
        } catch(e) {
            msg = locale.language
        }

        out << render(template: '/taglib/flagSelectorLandingPage', model: [
                locales: localeService.listLocales(),
                currentLocale: [prefix: locale.language, message: msg]
        ])
    }

    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
}
