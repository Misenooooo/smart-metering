<li class="dropdown language-dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><asset:image src="flags/${currentLocale.prefix}.png" width="20" height="20"/> ${currentLocale.message} <i class="fa fa-chevron-down"></i></a>
    <ul class="dropdown-menu dropdown-center language-menu">
        <g:each in="${locales}" var="locale">
            <g:if test="${locale != currentLocale}">
                <li>
                    <g:link controller="${params.controller}" action="${params.action in Map ? null : params.action}" params="${params + [lang: locale.prefix]}" class="menuButton"><div class="flag"><asset:image src="flags/${locale.prefix}.png" width="20" height="20"/></div> ${locale.message}</g:link>
                </li>
            </g:if>
        </g:each>
    </ul>
</li>

