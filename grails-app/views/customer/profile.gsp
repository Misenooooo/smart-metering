<!doctype html>
<html>
<head>
    <meta name="layout" content="customer"/>
    <script type="text/javascript">


        $(document).ready(function () {
            getSelectedDistributor();
        });

        var selectedDistributor;
        var selectedTariff;

        function getSelectedDistributor() {
            var URL="${createLink(controller:'CustomerProfileRest',action:'getSelectedDistributor')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    selectedDistributor = response.id;
                    getSelectedTariff(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function getSelectedTariff() {
            var URL="${createLink(controller:'CustomerProfileRest',action:'getSelectedTariff')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    selectedTariff = response.id;
                    getAllDistributors(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function getAllDistributors(){
            var URL="${createLink(controller:'CustomerProfileRest',action:'getAllDistributors')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    setAllDistributors(response);
                    if(selectedDistributor){
                        getAllTariffsForDistributor();
                    }
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }


        function setAllDistributors(response) {
            var select = $('#distributors');
            for (var i = 0; i < response.length; i++){
                var option = $('<option>').attr('value',response[i].id).html(response[i].name);
                if(selectedDistributor === response[i].id) {
                    option.attr('selected','selected');
                }
                select.append(option);
            }
        }

        function getAllTariffsForDistributor() {
            selectedDistributor = $('#distributors').val();
            var URL="${createLink(controller:'CustomerProfileRest',action:'getAllTariffsForDistributor')}";
            var data={'selectedDistributor':selectedDistributor};
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    setAllTariffsForDistributor(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function setAllTariffsForDistributor(response) {
            var select = $('<select>').attr('id','tariffs').addClass('styled-select slate');
            var label = $('<label>').attr('for','tariffs').html(tariffProfileMessage);
            for (var i = 0; i < response.length; i++){
                var option = $('<option>').attr('value',response[i].id).html(response[i].name);
                if(selectedTariff === response[i].id) {
                    option.attr('selected','selected');
                }
                select.append(option);
            }
            var wrapper = $('#tariffs-wrapper');
            wrapper.empty().append(label    );
            wrapper.append(select);
        }

        function save() {
            var URL="${createLink(controller:'Customer',action:'updateAccommodationUnit')}";
            selectedDistributor = $('#distributors').val();
            selectedTariff = $('#tariffs').val();
            var data={'selectedTariff':selectedTariff};
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    $("#message-tariff").show();
                    setTimeout(function() { $("#message-tariff").hide(); }, 5000)
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }
    </script>
</head>
<body>
<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="customer.profile.title"/></h1>
    <h3 class="sub-heading text-center"><g:message code="customer.profile.subTitle"/></h3>
</section>
<section class="profile">
    <div class="container-fluid">
            <div class="row">
            <div id="tariff-form-wrapper" class="col-md-6">
                <div class="img-header">
                    <img src="${assetPath(src: 'profile/house.jpg')}" />
                    <h2 id="household-title"><g:message code="customer.profile.household.title"/></h2>
                </div>

                <form class="form">
                    <div class="col-sm-12">
                        <div id="message-tariff" class="alert alert-success text-center" style="display: none">${g.message(code: 'customer.profile.successfully.updated')}</div>
                    </div>
                    <div class="col-sm-12">
                        <div id="distributors-wrapper" class="form-group" >
                            <label for="distributors" class="text-center"><g:message code="customer.profile.household.distributors"/></label>
                            <select class="styled-select slate" id="distributors" onchange="getAllTariffsForDistributor()"></select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div id="tariffs-wrapper" class="form-group">

                        </div>
                    </div>
                    <div class="button-wrapper col-sm-12" >
                        <button class="btn btn-lg btn-success profile-button" onclick="save();return false"><g:message code="customer.profile.household.save"/></button>
                    </div>
                </form>
            </div>
            <div id="user-profile" class="col-md-6">
                <div class="img-header" >
                    <img src="${assetPath(src: 'profile/user.jpg')}" />
                    <h2><g:message code="customer.profile.user.title"/></h2>
                </div>
                <g:if test="${email}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-warning text-center" role="alert">${email}</div>
                        </div>
                    </div>
                </g:if>
                <g:if test="${success}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success text-center" >${success}</div>
                        </div>
                    </div>
                </g:if>
                <g:hasErrors bean="${user}">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${user}" var="error">
                                    <g:if test="${error.field != "username"}">
                                        <li class="text-center" <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                    </g:if>
                                </g:eachError>
                            </ul>
                        </div>
                    </div>
                </g:hasErrors>
                <g:form action="updateUser">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="username"><g:message code="customer.profile.user.userName"/></label>
                            <input type="text"  class="form-control" id="username" name="username" placeholder="<g:message code="customer.profile.user.userName.placeholder"/>" value="${user.username}">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="firstName"><g:message code="customer.profile.user.firstName"/></label>
                            <input type="text"  class="form-control" id="firstName" name="firstName" placeholder="<g:message code="customer.profile.user.firstName.placeholder"/>" value="${fieldValue(bean:user,field:'firstName')}">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="lastName"><g:message code="customer.profile.user.lastName"/></label>
                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="<g:message code="customer.profile.user.lastName.placeholder"/>" value="${fieldValue(bean:user,field:'lastName')}">
                        </div>
                    </div>

                    <div class="text-center">
                        <g:submitButton name="submit" class="btn btn-lg btn-success" value="${message(code: 'customer.profile.user.save')}" />
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</section>
</body>
</html>
