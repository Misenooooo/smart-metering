<!doctype html>
<html>
<head>
    <meta name="layout" content="customer"/>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">


        google.charts.load('current', {
            callback: getComparisonDataForBranches,
            packages:['annotationchart', 'corechart']
        });


        var annotationChart;
        var annotationDataTable;

        var averagesForBranch;

        var appliancesPieChart;
        var roomsPieChart;

        function getComparisonDataForBranches() {
            $(document).ready(function () {
                var URL = "${createLink(controller:'CustomerComparisonRest',action:'getComparisonDataForBranches')}";
                $.ajax({
                    url: URL,
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        createGraphForAppliances(response);
                        createGraphForRooms(response);
                        getAllBranchesWithData();
                    },
                    error: function (response) {
                        console.log("error " + response);
                    }

                });
            });
        }

        function createGraphForAppliances(response) {
            var data= [];
            data.push(['Comparison of my appliances', 'kWh of total']);
            for (var i = 0; i < response.length; i++){
                if(response[i].type === 'appliance'){
                    var message = response[i].name.replace(/[a-z,A-Z]/g,"")+ ". " + applianceMessages[response[i].name.replace(/[0-9]/g,"")];
                    data.push([message,response[i].value]);
                }
            }
            var dataTable = google.visualization.arrayToDataTable(data);
            var options = {
                title: titleComparisonAppliancesMessage,
                is3D: true

            };
            appliancesPieChart = new google.visualization.PieChart(document.getElementById('appliances-pie-chart'));
            appliancesPieChart.draw(dataTable, options);
        }

        function createGraphForRooms(response) {
            var data= [];
            data.push(['Comparison of my rooms', 'kWh of total']);
            for (var i = 0; i < response.length; i++){
                if(response[i].type === 'room'){
                    var message = response[i].name.replace(/[a-z,A-Z]/g,"")+ ". " + roomMessages[response[i].name.replace(/[0-9]/g,"")];
                    data.push([message,response[i].value]);
                }
            }
            var dataTable = google.visualization.arrayToDataTable(data);

            var options = {
                title: titleComparisonRoomsMessage,
                is3D: true
            };
            roomsPieChart = new google.visualization.PieChart(document.getElementById('rooms-pie-chart'));
            roomsPieChart.draw(dataTable, options);
        }



        function getAllBranchesWithData() {
            var URL="${createLink(controller:'CustomerComparisonRest', action:'getAllBranchesWithData')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    setAppliances(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }

        function setAppliances(response) {
            var select = $('#branches');
            select.empty();
            for (var i = 0; i < response.length; i++){
                var text = roomMessages[response[i].name.replace(/[0-9]/g,"")];
                if(!text){
                    text = applianceMessages[response[i].name.replace(/[0-9]/g,"")];
                }
                text = response[i].name.replace(/[a-z,A-Z]/g,"") + '. '+ text;
                var option = $('<option>').addClass('branch-option').attr('value',response[i].name).html(text);
                select.append(option)
            }
            recalculate();
        }
        
        function recalculate() {
            var URL="${createLink(controller:'CustomerComparisonRest', action:'getAverageForSelectedBranch')}";
            var data = {'branch':$('.branch-option:checked').val()};
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    averagesForBranch = response;
                    getDataForSelectedBranch();
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }

        function getDataForSelectedBranch() {
            var URL="${createLink(controller:'CustomerComparisonRest', action:'getAllConsumptionDataForBranch')}";
            var data = {'branch':$('.branch-option:checked').val()};
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    drawChart(response)
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }
        
        function drawChart(response) {
            if(annotationChart){
                annotationChart.clearChart();
            }
            annotationDataTable = new google.visualization.DataTable();
            annotationDataTable.addColumn('date', graphDateMessage);
            annotationDataTable.addColumn('number', graphComparisonMyMessage);
            annotationDataTable.addColumn('number', graphComparisonAvgMessage);
            for (var i = 0; i < response.length; i++){
                var date = new Date(response[i].year,response[i].month,response[i].day);
                annotationDataTable.addRow([date, parseFloat(response[i].consumption), parseFloat(averagesForBranch[i])]);
            }
            annotationChart = new google.visualization.AnnotationChart(document.getElementById('chart_div'));
            var options = {
                displayAnnotations: true,
                allValuesSuffix: "kWh"
            };
            annotationChart.draw(annotationDataTable, options);
        }


    </script>
</head>
<body>

<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="customer.comparison.title"/></h1>
    <h3 class="sub-heading text-center"><g:message code="customer.comparison.subtitle"/></h3>
</section>
<section class="section-chart">
    <div class="pie-charts container-fluid">
        <div class="col-sm-6">
            <h2 class="text-center"><g:message code="customer.comparison.appliances.title"/></h2>
            <div id="appliances-pie-chart" style='width: 100%;'>

            </div>
        </div>
        <div class="col-sm-6">
            <h2 class="text-center"><g:message code="customer.comparison.rooms.title"/></h2>
            <div id="rooms-pie-chart" style='width: 100%;'>

            </div>
        </div>
    </div>
    <h2 class="text-center"><g:message code="customer.comparison.neighbours.title"/></h2>
    <div class="branches-wrapper">
        <select class="form-control" id="branches" onchange="recalculate()"></select>
    </div>
    <div id='chart_div' style='width: 80%;'></div>

</section>

</body>
</html>
