<!doctype html>
<html>
<head>
    <meta name="layout" content="customer"/>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">


        google.charts.load('current', {
            callback: getDistributor,
            packages: ['corechart', 'line']
        });

        Date.prototype.addDays = function(days) {
            var dat = new Date(this.valueOf());
            dat.setDate(dat.getDate() + days);
            return dat;
        };

        window.grailsSupport = {
            assetsRoot : '${ raw(asset.assetPath(src: '')) }'
        };

        var annotationChart;
        var annotationDataTable;

        var appliances = {};

        var selectedAppliance;
        var dataForAppliance;
        var consumptionPerDay;

        var offers;

        var selectedDistributor;
        var selectedTariff;
        var selectedHighRate;
        var selectedLowRate;

        var yearsToCalculate = 5;

        function getDistributor() {
            var URL="${createLink(controller:'CustomerProfileRest', action:'getSelectedDistributor')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    selectedDistributor = response.name;
                    getTariff();
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }

        function getTariff() {
            var URL="${createLink(controller:'CustomerProfileRest', action:'getSelectedTariff')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    selectedTariff = response.name;
                    selectedHighRate = response.highRate;
                    selectedLowRate = response.lowRate;
                    setTariff();
                    getAllAppliancesInHome();
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }

        function setTariff() {
            var distributor = $('<h4>').addClass('text-center').html(distributorMessage + ' <strong>' + selectedDistributor + '<strong>');
            distributor.addClass('first-line').addClass('first');
            var tariff = $('<h4>').addClass('text-center').html(tariffMessage + ' <strong>' + selectedTariff+ '</strong>');
            tariff.attr('id','tariff-line').addClass('first-line').addClass('second');
            var highRate = $('<h4>').addClass('text-center').html(highRateMessage + ' <strong>' + selectedHighRate + '</strong> kWh');
            highRate.addClass('second-line').addClass('first');
            var lowRate = $('<h4>').addClass('text-center').html(lowRateMessage + ' <strong>' + selectedLowRate + '</strong> kWh');
            lowRate.addClass('second-line').addClass('second');
            var wrapper = $('#tariff');
            wrapper.empty().append(distributor);
            wrapper.append(tariff);
            wrapper.append(highRate);
            if(selectedLowRate) {
                wrapper.append(lowRate);
            }
        }

        function getAllAppliancesInHome() {
            var URL="${createLink(controller:'customerDataPortRest', action:'getAllAppliances')}";
            $.ajax({
                url:URL,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    setAppliancesInHome(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function setAppliancesInHome(response) {
            if(appliances){
                appliances = {};
            }
            for (var i = 0; i < response.length; i++){
                appliances[response[i].applianceName] = response[i].applianceName;
            }

            var select = $('<select class="styled-select slate" onchange="applianceSelected(this.value)" name="selectedAppliance" id="selectedAppliance"></select>');
            var selected = false;
            $.each(appliances, function(key, value)
            {
                if(!selected){
                    selected = true;
                    option = $('<option selected ></option>');
                    applianceSelected(value);
                } else {
                    option = $('<option></option>');
                }
                option.attr('value', value);
                option.text(value.replace(/[a-z,A-Z]/g,"") + '. ' + applianceMessages[value.replace(/[0-9]/g,"")]);
                select.append(option);
            });
            $('#appliance-select-container').empty().append(select);
        }

        function applianceSelected(appliance) {
            selectedAppliance = appliance;
            getAllAppliancesOffers();
        }


        function getAllAppliancesOffers() {
            var URL="${createLink(controller:'customerCalculatorRest', action:'getAllAppliancesForSelectedType')}";
            var data={'selectedApplianceType':selectedAppliance.replace(/[0-9]/g,"")};
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    offers = response;
                    setAppliancesOffers(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }

        function setAppliancesOffers(response) {
            var div = $("<div>").attr("id", "offers").addClass("appliances-offers");
            var ul = $("<ul>").addClass("offers");
            for (var i = 0; i < response.length; i++){
                var li;
                if(i%2) {
                    li = $('<li>').addClass("offer").addClass("no-bullet").addClass("white-background");

                } else {
                    li = $('<li>').addClass("offer").addClass("no-bullet").addClass("gray-background");
                }
                var radio = $('<input type="radio" name="target" onclick="getConsumptionData()">').attr('value',i);
                if(!i){
                    radio.attr('checked','checked');
                }
                var img = $('<img>').attr('src',window.grailsSupport.assetsRoot+'offers'+response[i].imagePath);
                var imageWrapper = $('<div>').addClass('image-wrapper');
                var descriptionWrapper = $('<div>').addClass('description-wrapper');
                var name = $('<h3>').html(response[i].name).addClass("text-center");
                var yearlyConsumption = $('<h3>').html( resultAnnualConsumptionMessage + " " + (Math.round(response[i].yearlyConsumption * 100) / 100)
                        + " kWh " + perYearMessage ).addClass("text-center");
                var price = $('<h3>').html(resultPriceMessage+" " + (Math.round(response[i].price * 100) / 100) + " &euro;" ).addClass("text-center");
                descriptionWrapper.append(name);
                descriptionWrapper.append(price);
                descriptionWrapper.append(yearlyConsumption);
                descriptionWrapper.append(radio);
                imageWrapper.append(img);
                li.append(imageWrapper);
                li.append(descriptionWrapper);
                ul.append(li);
            }
            div.append(ul);
            $('#appliances').empty().append(div);
            if(response.length) {
                getConsumptionData()
            } else {
                var h2 = $('<h2>').html(noOffersFoundMessage).addClass("text-center");
                $('#appliances').append(h2);
            }
        }

        function getConsumptionData() {
            if(dataForAppliance == selectedAppliance){
                recalculate();
                return;
            }
            var URL="${createLink(controller:'customerCalculatorRest', action:'getAllConsumptionDataWithPriceForAppliance')}";
            var data={'applianceName':selectedAppliance};
            dataForAppliance = selectedAppliance;
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    consumptionPerDay = response;
                    recalculate();
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }


        function recalculate(){
            var selectedOfferYearPrice = getSelectedOfferYearPrice();
            var selectedOfferPrice = getSelectedOfferPrice();
            var selectedApplianceYearPrice = getSelectedApplianceYearPrice();
            visualizeCalculation(selectedOfferYearPrice, selectedOfferPrice, selectedApplianceYearPrice);
        }

        function getSelectedApplianceYearPrice() {
            var sum = 0.0;
            for (var i = 0; i < consumptionPerDay.length; i++){
                if(!selectedLowRate) {
                    sum = sum + consumptionPerDay[i] * selectedHighRate;
                } else {
                    sum = sum + (consumptionPerDay[i] * selectedHighRate * 0.33) + (consumptionPerDay[i] * selectedHighRate * 0.67);
                }
            }
            return sum;
        }

        function getSelectedOfferPrice() {
            var selectedOfferIndex = $('input[name=target]:checked').val();
            return offers[selectedOfferIndex].price;
        }

        function getSelectedOfferYearPrice(){
            var pricePerKwH;
            var tarif = $('input[name=tarif]:checked').val();
            if(!selectedLowRate){
                pricePerKwH = selectedHighRate;
            } else{
                pricePerKwH = selectedLowRate * 0.33 + selectedHighRate * 0.67;
            }
            var selectedOfferIndex = $('input[name=target]:checked').val();
            return offers[selectedOfferIndex].yearlyConsumption * pricePerKwH;
        }

        function visualizeCalculation(selectedOfferYearPrice, selectedOfferPrice, selectedApplianceYearPrice) {
            var spanOfReturnTime;
            var div = $('#result-wrapper');
            div.empty();
            var h2 = $('<h2>').html(calculationHeadingMessage).addClass('text-center');
            $('#result-title').empty().append(h2);

            if(selectedOfferYearPrice < selectedApplianceYearPrice) {
                spanOfReturnTime = selectedOfferPrice / (selectedApplianceYearPrice - selectedOfferYearPrice);
                var returnTime = $('<h4>').html(calculationReturnMessage + " <strong>" + (Math.round(spanOfReturnTime * 100) / 100) + " " + calculationYearMessage + "</strong>").addClass('text-center');
                var annualSave = (Math.round((selectedApplianceYearPrice - selectedOfferYearPrice) * 100) / 100);
                var annualSaving = $('<h4>').html(differencePerYearMessage + " <strong>" + annualSave  + " &euro; " + perYearMessage + "</strong>").addClass("text-center");
                div.append(returnTime);
                div.append(annualSaving);
            } else {
                var neverReturn = $('<h4>').html(neverReturnMessage).addClass('text-center');
                div.append(neverReturn);
            }

            createGraph(selectedOfferYearPrice, selectedOfferPrice, selectedApplianceYearPrice);
        }

        function createGraph(selectedOfferYearPrice, selectedOfferPrice, selectedApplianceYearPrice) {
            if(annotationChart){
                annotationChart.clearChart();
            }

            var daysToCalculate = yearsToCalculate * 365;
            annotationDataTable = new google.visualization.DataTable();
            annotationDataTable.addColumn('date', graphDateMessage);
            annotationDataTable.addColumn('number', graphExistingApplianceMessage);
            annotationDataTable.addColumn('number', graphSelectedApplianceMessage);

            var existingAppliance = 0;
            var selectedAppliance = selectedOfferPrice;
            var existingApplianceIncrement = selectedApplianceYearPrice / 365;
            var selectedApplianceIncrement = selectedOfferYearPrice / 365;
            for (var j = 0; j < daysToCalculate; j++){
                var date = new Date();
                date = date.addDays(j);
                existingAppliance = existingAppliance + existingApplianceIncrement;
                selectedAppliance = selectedAppliance + selectedApplianceIncrement;
                annotationDataTable.addRow([date, existingAppliance, selectedAppliance]);
            }
            $('#chart-title').empty().append(graphCalculatorMessage).show();
            annotationChart = new google.visualization.LineChart(document.getElementById('chart_div'));
            var options = {
                hAxis: {
                    title: graphHaxisMessage
                },
                vAxis: {
                    title: graphYaxisMessage
                },
                crosshair: {
                    color: '#000',
                    trigger: 'selection'
                }
            };
            annotationChart.draw(annotationDataTable, options);
        }

    </script>
    <script type="text/javascript" src="assets/customer/calculator.js"></script>
</head>
<body>
<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="customer.calculator.title"/></h1>
    <h3 class="sub-heading text-center"><g:message code="customer.calculator.subtitle"/></h3>
</section>
<section class="calculator">
    <div class="col-sm-6 col-xs-12">
        <h2 class="text-center" ><g:message code="customer.calculator.settings.title"/></h2>
        <div id="tariff"></div>
        <h2 class="text-center" style="margin-top: 40px"><g:message code="customer.calculator.appliance.select"/></h2>
        <div id="appliance-select-container" class="white-background"></div>
        <div id="result-title" style="width: 100%"></div>
        <div id="result-wrapper"></div>
        <h2 id="chart-title" class="text-center" style="width: 100%; display: none"></h2>
        <div id='chart_div' style='width: 100%;'></div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <h2 class="text-center"><g:message code="customer.calculator.appliances.title"/></h2>
        <div id="appliances"></div>
    </div>
</section>
</body>
</html>
