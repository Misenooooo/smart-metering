<!doctype html>
<html>
<head>
    <meta name="layout" content="customer"/>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">


        google.charts.load('current', {
            callback: getTariff,
            packages:['annotationchart', 'calendar']
        });

        var calendarChart;
        var calendarDataTable;

        var annotationChart;
        var annotationDataTable;

        var selectedHighRate;
        var selectedLowRate;

        function getTariff() {
            var URL="${createLink(controller:'CustomerProfileRest', action:'getSelectedTariff')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    selectedHighRate = response.highRate;
                    selectedLowRate = response.lowRate;
                    getAllConsumption()
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function getAllConsumption(){
            var URL="${createLink(controller:'customerDataPortRest',action:'getAllConsumptionDataForCalendar')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    createGraphForAllConsumption(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }
            });
        }

        function createGraphForAllConsumption(response) {
            if(calendarChart){
                calendarChart.clearChart();
            }
            calendarDataTable = new google.visualization.DataTable();
            calendarDataTable.addColumn({ type: 'date', id: graphDateMessage });
            var option = $('#data-type').val();
            if(option == 'consumption') {
                calendarDataTable.addColumn('number', graphConsumptionMessage);
            } else {
                calendarDataTable.addColumn('number', graphPriceMessage);
            }
            for (var i = 0; i < response.length; i++){
                var date = new Date(response[i].year,response[i].month,response[i].day);
                var dailyConsumption = parseFloat(response[i].consumption);
                if(option == 'consumption') {
                    calendarDataTable.addRow([date, dailyConsumption]);
                } else if (selectedLowRate) {
                    calendarDataTable.addRow([date, dailyConsumption * selectedLowRate * 0.33 + dailyConsumption * selectedHighRate * 0.67]);
                } else {
                    calendarDataTable.addRow([date, dailyConsumption * selectedHighRate]);
                }
            }

            calendarChart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

            var title;
            if(option == 'consumption'){
                title = titleIndexConsumptionMessage;
            } else {
                title = titleIndexPriceMessage;
            }

            var options = {
                title: title,
                height: 350
            };

            google.visualization.events.addListener(calendarChart, 'select', getDayConsumption);

            calendarChart.draw(calendarDataTable, options);
        }


        function getDayConsumption() {
            var date = calendarDataTable.getValue(calendarChart.getSelection()[0].row, 0);
            if (date) {
                $(document).ready(function () {
                    var URL = "${createLink(controller:'customerDataPortRest',action:'getAllDailyConsumptionData')}";
                    data = {'date': date};
                    $.ajax({
                        url: URL,
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            createGraphForDailyConsumption(response);
                        },
                        error: function (response) {
                            console.log("error " + response);
                        }

                    });
                });
            }
        }

        function createGraphForDailyConsumption(response) {
            if(annotationChart){
                annotationChart.clearChart();
            }
            annotationDataTable = new google.visualization.DataTable();
            annotationDataTable.addColumn('date', graphDateMessage);
            annotationDataTable.addColumn('number', graphConsumptionMessage);
            for (var i = 0; i < response.length; i++){
                var date = new Date(response[i].year,response[i].month,response[i].day,response[i].hour,response[i].minute,response[i].second);
                annotationDataTable.addRow([date, parseFloat(response[i].consumption)]);
            }
            annotationChart = new google.visualization.AnnotationChart(document.getElementById('chart_div'));
            $('#chart-title').show();
            var options = {
                displayAnnotations: true,
                allValuesSuffix: "kW"
            };
            annotationChart.draw(annotationDataTable, options);
        }
    </script>
</head>
<body>

<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="customer.index.title"/></h1>
</section>
<section class="select">
    <h2 class="text-center">${g.message(code: 'customer.index.select.title')}</h2>
    <div id="data-type-wrapper" class="container-fluid">
        <select class="styled-select slate" id="data-type" onchange="getAllConsumption()">
            <option value="consumption"><g:message code="customer.index.consumption"/></option>
            <option value="price"><g:message code="customer.index.price"/></option>
        </select>
    </div>
</section>
<section class="section-calendar">
    <div id="calendar_basic" style='width: 80%;'></div>
</section>
<section class="section-chart">
    <h3  id="chart-title" class="text-center" style="display: none">${g.message(code: 'customer.index.consumption.daily.title')}</h3>
    <div id='chart_div' style='width: 80%;'></div>
</section>

</body>
</html>
