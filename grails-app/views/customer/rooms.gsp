<!doctype html>
<html>
<head>
    <meta name="layout" content="customer"/>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">


        window.grailsSupport = {
            bathroom : '${assetPath(src: 'rooms/bathroom.jpg')}',
            bedroom : '${assetPath(src: 'rooms/bedroom.jpg')}',
            diningroom : '${assetPath(src: 'rooms/diningroom.jpg')}',
            garage : '${assetPath(src: 'rooms/garage.jpg')}',
            kitchen : '${assetPath(src: 'rooms/kitchen.jpg')}',
            livingroom : '${assetPath(src: 'rooms/livingroom.jpg')}',
            pool : '${assetPath(src: 'rooms/pool.jpg')}',
            security : '${assetPath(src: 'rooms/security.jpg')}',
            utilityroom : '${assetPath(src: 'rooms/utilityroom.png')}'
        };

        google.charts.load('current', {
            callback: getTariff(),
            packages:['annotationchart', 'calendar']
        });
        var calendarChart;
        var calendarDataTable;

        var annotationChart;
        var annotationDataTable;

        var rooms = {}; /*This is a map*/
        var selectedRoom;

        var selectedHighRate;
        var selectedLowRate;

        function getTariff() {
            var URL="${createLink(controller:'CustomerProfileRest', action:'getSelectedTariff')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    selectedHighRate = response.highRate;
                    selectedLowRate = response.lowRate;
                    getAllRooms()
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function getAllRooms() {
            var URL="${createLink(controller:'customerDataPortRest', action:'getAllRooms')}";
            $.ajax({
                url:URL,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    setRooms(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function setRooms(response) {
            if(rooms){
                rooms = {};
            }
            console.log(response);
            for (var i = 0; i < response.length; i++){
                rooms[response[i].roomName] = response[i].roomName;
            }


            var output = [];
            $.each(rooms, function(key, value)
            {
                output.push('<div class="col-lg-6 col-xs-12 room-wrapper">'
                        + '<h3>'
                        +  value.replace(/[a-z,A-Z]/g,"") + '. '  + roomMessages[value.replace(/[0-9]/g,"")]
                        + '</h3>'
                        + '<img class="room" '
                        + 'id='+'"'+value+'"'
                        + 'src='+'"'+window.grailsSupport[value.replace(/[0-9]/g,"")]+'"/>' +
                        '</div>');
            });
            $('#rooms-container').html(output.join(''));
            $.each(rooms, function(key, value)
            {
                $(document).on('click', '#'+value, function(){
                    selectedRoom = this.id;
                    getAllConsumption();
                });
            })
        }

        $(document).ready(function() {
            $("#mySelect").change(function () {
                selectedRoom = $(this).val();
                if(selectedRoom){
                    getAllConsumption();
                }
            });
        });

        function getAllConsumption(){
            if(!selectedRoom){
                return;
            }
            var URL="${createLink(controller:'customerDataPortRest',action:'getAllConsumptionDataForAppliance')}";
            var data={'selectedAppliance':selectedRoom};
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    createGraphForAllConsumption(response);

                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function createGraphForAllConsumption(response) {
            if(calendarChart){
                calendarChart.clearChart();
                $('#chart_div').empty();
            }
            calendarDataTable = new google.visualization.DataTable();
            calendarDataTable.addColumn({ type: 'date', graphDateMessage });
            var option = $('#data-type').val();
            if(option == 'consumption') {
                calendarDataTable.addColumn('number', graphConsumptionMessage);
            } else {
                calendarDataTable.addColumn('number', graphPriceMessage);
            }
            for (var i = 0; i < response.length; i++){
                var date = new Date(response[i].year,response[i].month,response[i].day);
                var dailyConsumption = parseFloat(response[i].consumption);
                if(option == 'consumption') {
                    calendarDataTable.addRow([date, dailyConsumption]);
                } else if (selectedLowRate) {
                    calendarDataTable.addRow([date, dailyConsumption * selectedLowRate * 0.33 + dailyConsumption * selectedHighRate * 0.67]);
                } else {
                    calendarDataTable.addRow([date, dailyConsumption * selectedHighRate]);
                }
            }
            calendarChart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

            var title;
            if(option == 'consumption'){
                title = titleRoomsConsumptionMessage;
            } else {
                title = titleRoomsPriceMessage;
            }
            var options = {
                title: title,
                height: 350
            };
            google.visualization.events.addListener(calendarChart, 'select', getDayConsumption);
            calendarChart.draw(calendarDataTable, options);
        }


        function getDayConsumption() {
            var date = calendarDataTable.getValue(calendarChart.getSelection()[0].row, 0);
            if (date) {
                $(document).ready(function () {
                    var URL = "${createLink(controller:'customerDataPortRest',action:'getAllDailyConsumptionDataForAppliance')}";
                    var data = {'date': date, 'applianceName':selectedRoom};
                    $.ajax({
                        url: URL,
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            createGraphForDailyConsumption(response);
                        },
                        error: function (response) {
                            console.log("error " + response);
                        }

                    });
                });
            }
        }

        function createGraphForDailyConsumption(response) {
            if(annotationChart){
                annotationChart.clearChart();
            }
            annotationDataTable = new google.visualization.DataTable();
            annotationDataTable.addColumn('date', graphDateMessage);
            annotationDataTable.addColumn('number', graphConsumptionMessage);
            for (var i = 0; i < response.length; i++){
                var date = new Date(response[i].year,response[i].month,response[i].day,response[i].hour,response[i].minute,response[i].second);
                annotationDataTable.addRow([date, parseFloat(response[i].consumption)]);
            }

            annotationChart = new google.visualization.AnnotationChart(document.getElementById('chart_div'));
            var options = {
                displayAnnotations: true,
                allValuesSuffix: "kW"
            };

            $('#chart-title').show();
            annotationChart.draw(annotationDataTable, options);
        }
    </script>
</head>
<body>

<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="customer.rooms.title"/></h1>
    <h3 class="sub-heading text-center"><g:message code="customer.rooms.subtitle"/></h3>
</section>
<section class="select">
    <h2 class="text-center">${g.message(code: 'customer.index.select.title')}</h2>
    <div id="data-type-wrapper" class="container-fluid">
        <select class="styled-select slate" id="data-type" onchange="getAllConsumption()">
            <option value="consumption"><g:message code="customer.index.consumption"/></option>
            <option value="price"><g:message code="customer.index.price"/></option>
        </select>
    </div>
</section>
<section class="section-calendar">
    <div id="calendar_basic" style='width: 80%;'></div>
</section>

<section class="section-chart">
    <h3  id="chart-title" class="text-center" style="display: none">${g.message(code: 'customer.rooms.consumption.daily.title')}</h3>
    <div id='chart_div' style='width: 80%;'></div>
</section>

<section>
    <h2  id="rooms-title" class="text-center">${g.message(code: 'customer.rooms.list.title')}</h2>
    <div class="container-fluid">
        <div id="rooms-container" class="images">

        </div>
    </div>
</section>

</body>
</html>
