<!doctype html>
<html>
<head>
    <meta name="layout" content="customer"/>
    <title><g:message code="customer.pairing.title"/></title>
</head>
<body>
<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="customer.pairing.title"/></h1>
    <h3 class="sub-heading text-center"><g:message code="customer.pairing.subTitle"/></h3>
</section>
<section class="pairing">
    <div id="pairing" class="form-wrapper">

        <g:form action="pairAccounts">
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <div class="form-group">
                <label for="code" class="required"><g:message code="customer.pairing.code.label"/></label>
                <input type="text" required class="form-control" id="code" name="code" placeholder="<g:message code="customer.pairing.code.placeholder"/>" value="${code}">
            </div>
            <div class="button-wrapper" class="text-center">
                <g:submitButton name="pair" class="btn btn-block btn-primary" value="${message(code: 'customer.pairing.button')}" />
            </div>
        </g:form>
    </div>
</section>
</body>
</html>
