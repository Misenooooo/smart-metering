<!doctype html>
<html>
<head>
    <meta name="layout" content="customer"/>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        window.grailsSupport = {
            air : '${assetPath(src: 'appliances/air-conditioner.png')}',
            airwindowunit : '${assetPath(src: 'appliances/air-compressor.jpg')}',
            clotheswasher : '${assetPath(src: 'appliances/clotheswasher.jpg')}',
            clotheswasherDryg : '${assetPath(src: 'appliances/clotheswasher.jpg')}',
            furnace : '${assetPath(src: 'appliances/furnace.jpg')}',
            dishwasher : '${assetPath(src: 'appliances/dishwasher.jpg')}',
            aquarium : '${assetPath(src: 'appliances/aquarium.jpg')}',
            disposal : '${assetPath(src: 'appliances/disposal.jpg')}',
            drye : '${assetPath(src: 'appliances/dryer.jpg')}',
            dryg : '${assetPath(src: 'appliances/dryer.jpg')}',
            freezer : '${assetPath(src: 'appliances/freezer.jpg')}',
            heater : '${assetPath(src: 'appliances/heater.jpg')}',
            housefan : '${assetPath(src: 'appliances/housefan.jpg')}',
            jacuzzi : '${assetPath(src: 'appliances/jacuzzi.jpg')}',
            icemaker : '${assetPath(src: 'appliances/icemaker.jpg')}',
            lightsPlugs : '${assetPath(src: 'appliances/lights_plugs.jpg')}',
            outsidelightsPlugs : '${assetPath(src: 'appliances/outsidelights_plugs.jpg')}',
            microwave : '${assetPath(src: 'appliances/microwave.jpg')}',
            oven : '${assetPath(src: 'appliances/oven.jpg')}',
            pool : '${assetPath(src: 'appliances/pool.jpg')}',
            poollight : '${assetPath(src: 'appliances/poollight.jpg')}',
            pump : '${assetPath(src: 'appliances/pump.jpg')}',
            range : '${assetPath(src: 'appliances/range.jpg')}',
            sprinkler : '${assetPath(src: 'appliances/sprinkler.jpg')}',
            refrigerator : '${assetPath(src: 'appliances/refrigerator.jpg')}',
            waterheater : '${assetPath(src: 'appliances/waterheater.jpg')}',
            winecooler : '${assetPath(src: 'appliances/winecooler.jpg')}'
        };


        google.charts.load('current', {
            callback: getTariff(),
            packages:['annotationchart', 'calendar']
        });

        var calendarChart;
        var calendarDataTable;

        var annotationChart;
        var annotationDataTable;

        var appliances = {};
        var selectedAppliance;
        var selectedDate;

        var selectedHighRate;
        var selectedLowRate;

        function getTariff() {
            var URL="${createLink(controller:'CustomerProfileRest', action:'getSelectedTariff')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    selectedHighRate = response.highRate;
                    selectedLowRate = response.lowRate;
                    getAllAppliances();
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function getAllAppliances() {
            var URL="${createLink(controller:'customerDataPortRest', action:'getAllAppliances')}";
            $.ajax({
                url:URL,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    setAppliances(response);
                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function setAppliances(response) {
            if(appliances){
                appliances = {};
            }
            for (var i = 0; i < response.length; i++){
                appliances[response[i].applianceName] = response[i].applianceName;
            }

            var output = [];
            $.each(appliances, function(key, value)
            {
                output.push('<div class="col-lg-4 col-sm-6 col-xs-12 appliance-wrapper">'
                                + '<h3>'
                                +  value.replace(/[a-z,A-Z]/g,"") + '. '+ applianceMessages[value.replace(/[0-9]/g,"")]
                                + '</h3>'
                                + '<img class="appliance" '
                                + 'id='+'"'+value+'"'
                                + 'src='+'"'+window.grailsSupport[value.replace(/[0-9]/g,"")]+'"/>' +
                            '</div>');
            });
            $('#appliances-container').html(output.join(''));
            $.each(appliances, function(key, value)
            {
                $(document).on('click', '#'+value, function(){
                    selectedAppliance = this.id;
                    getAllConsumption();
                });
            })
        }

        $(document).ready(function() {
            $("#mySelect").change(function () {
                selectedAppliance = $(this).val();
                if(selectedAppliance){
                    getAllConsumption();
                }
            });
        });

        function getAllConsumption(){
            if(!selectedAppliance){
                return;
            }
            var URL="${createLink(controller:'customerDataPortRest',action:'getAllConsumptionDataForAppliance')}";
            var data={'selectedAppliance':selectedAppliance};
            $.ajax({
                url:URL,
                type: "POST",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(response){
                    createGraphForAllConsumption(response);

                },
                error: function(response){
                    console.log("error "+ response);
                }

            });
        }

        function createGraphForAllConsumption(response) {
            if(calendarChart){
                calendarChart.clearChart();
                $('#chart_div').empty();
            }
            calendarDataTable = new google.visualization.DataTable();
            calendarDataTable.addColumn({ type: 'date', id: graphDateMessage });
            var option = $('#data-type').val();
            if(option == 'consumption') {
                calendarDataTable.addColumn('number', graphConsumptionMessage);
            } else {
                calendarDataTable.addColumn('number', graphPriceMessage);
            }
            for (var i = 0; i < response.length; i++){
                var date = new Date(response[i].year,response[i].month,response[i].day);
                var dailyConsumption = parseFloat(response[i].consumption);
                if(option == 'consumption') {
                    calendarDataTable.addRow([date, dailyConsumption]);
                } else if (selectedLowRate) {
                    calendarDataTable.addRow([date, dailyConsumption * selectedLowRate * 0.33 + dailyConsumption * selectedHighRate * 0.67]);
                } else {
                    calendarDataTable.addRow([date, dailyConsumption * selectedHighRate]);
                }
            }

            calendarChart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

            var title;
            if(option == 'consumption'){
                title = titleAppliancesConsumptionMessage;
            } else {
                title = titleAppliancesPriceMessage;
            }
            var options = {
                title: title,
                height: 350
            };

            google.visualization.events.addListener(calendarChart, 'select', getDayConsumption);

            calendarChart.draw(calendarDataTable, options);

        }


        function getDayConsumption() {
            selectedDate = calendarDataTable.getValue(calendarChart.getSelection()[0].row, 0);
            if (selectedDate) {
                $(document).ready(function () {
                    var URL = "${createLink(controller:'customerDataPortRest',action:'getAllDailyConsumptionDataForAppliance')}";
                    var data = {'date': selectedDate, 'applianceName':selectedAppliance};
                    $.ajax({
                        url: URL,
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            createGraphForDailyConsumption(response);
                        },
                        error: function (response) {
                            console.log("error " + response);
                        }

                    });
                });
            }
        }

        function createGraphForDailyConsumption(response) {
            if(annotationChart){
                annotationChart.clearChart();
            }
            annotationDataTable = new google.visualization.DataTable();
            annotationDataTable.addColumn('date', graphDateMessage);
            annotationDataTable.addColumn('number', graphConsumptionMessage);
            for (var i = 0; i < response.length; i++){
                var date = new Date(response[i].year,response[i].month,response[i].day,response[i].hour,response[i].minute,response[i].second);
                annotationDataTable.addRow([date, parseFloat(response[i].consumption)]);
            }
            $('#chart-title').show();
            annotationChart = new google.visualization.AnnotationChart(document.getElementById('chart_div'));
            var options = {
                displayAnnotations: true,
                allValuesSuffix: "kw"
            };
            annotationChart.draw(annotationDataTable, options);
        }



    </script>
</head>
<body>

<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="customer.appliances.title"/></h1>
    <h3 class="sub-heading text-center"><g:message code="customer.appliances.subtitle"/></h3>
</section>
<section class="select">
    <h2 class="text-center">${g.message(code: 'customer.index.select.title')}</h2>
    <div id="data-type-wrapper" class="container-fluid">
        <select class="styled-select slate" id="data-type" onchange="getAllConsumption()">
            <option value="consumption"><g:message code="customer.index.consumption"/></option>
            <option value="price"><g:message code="customer.index.price"/></option>
        </select>
    </div>
</section>
<section class="section-calendar">
    <div id="calendar_basic" style='width: 80%;'></div>
</section>

<section class="section-chart">
    <h3  id="chart-title" class="text-center" style="display: none">${g.message(code: 'customer.appliances.consumption.daily.title')}</h3>
    <div id='chart_div' style='width: 80%;'></div>
</section>

<section>
    <h2  id="appliance-title" class="text-center">${g.message(code: 'customer.appliances.list.title')}</h2>
    <div class="container-fluid">
        <div id="appliances-container" class="images">

        </div>
    </div>
</section>

</body>
</html>
