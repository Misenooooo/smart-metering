<html>
<head>
    <meta name='layout' content='frontend'/>
    <asset:stylesheet src="/frontend/login.css"/>
</head>

<body>
<section class="login-container">
    <div id="content">

        <h1><g:message code="springSecurity.login.header"/></h1>
        <g:if test="${flash.message}">
            <div class="alert alert-success" style="padding: 10px">${flash.message}</div>
        </g:if>
        <form action="/security/login_acccess_check" method='POST' id='loginForm'  autocomplete='on'>
            <div>
                <input type='text' name='security_username' id='security_username' class="login-username" value="${params.security_username}"
                       placeholder="${message(code: 'springSecurity.login.placeholder')}"/>
            </div>
            <div>
                <input type='password' name='security_password' class="login-password" id='password'
                       placeholder="${message(code: 'springSecurity.password.placeholder')}"/>
            </div>
            <div>
                <input type="submit" class="btn btn-block btn-primary" name="login" value="${message(code: "login.button")}" placeholder="${message(code: "login.button")}"/>
                <g:link controller="registration" action="registration" class="btn btn-success"><g:message code="register.button" /> </g:link>
            </div>
        </form>
    </div>
</section>
<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['security_username'].focus();
    })();
    // -->
</script>
</body>
</html>
