<!doctype html>
<html lang="en" class="no-js">
<head>
    <title><g:layoutTitle default="${message(code: "layout.main.title")}"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <asset:stylesheet src="manifests/customer.css"/>
    <script type="text/javascript">
    applianceMessages = {
    air : "${g.message(code: 'customer.appliances.air')}",
    airwindowunit : "${g.message(code: 'customer.appliances.airwindowunit')}",
    clotheswasher : "${g.message(code: 'customer.appliances.clotheswasher')}",
    clotheswasherDryg : "${g.message(code: 'customer.appliances.clotheswasherDryg')}",
    furnace : "${g.message(code: 'customer.appliances.furnace')}",
    dishwasher : "${g.message(code: 'customer.appliances.dishwasher')}",
    aquarium : "${g.message(code: 'customer.appliances.aquarium')}",
    disposal : "${g.message(code: 'customer.appliances.disposal')}",
    drye : "${g.message(code: 'customer.appliances.drye')}",
    dryg : "${g.message(code: 'customer.appliances.dryg')}",
    freezer : "${g.message(code: 'customer.appliances.freezer')}",
    heater : "${g.message(code: 'customer.appliances.heater')}",
    housefan : "${g.message(code: 'customer.appliances.housefan')}",
    jacuzzi : "${g.message(code: 'customer.appliances.jacuzzi')}",
    icemaker : "${g.message(code: 'customer.appliances.icemaker')}",
    lightsPlugs : "${g.message(code: 'customer.appliances.lightsPlugs')}",
    outsidelightsPlugs : "${g.message(code: 'customer.appliances.outsidelightsPlugs')}",
    microwave : "${g.message(code: 'customer.appliances.microwave')}",
    oven : "${g.message(code: 'customer.appliances.oven')}",
    pool : "${g.message(code: 'customer.appliances.pool')}",
    poollight : "${g.message(code: 'customer.appliances.poollight')}",
    pump : "${g.message(code: 'customer.appliances.pump')}",
    range : "${g.message(code: 'customer.appliances.range')}",
    sprinkler : "${g.message(code: 'customer.appliances.sprinkler')}",
    refrigerator : "${g.message(code: 'customer.appliances.refrigerator')}",
    waterheater : "${g.message(code: 'customer.appliances.waterheater')}",
    winecooler : "${g.message(code: 'customer.appliances.winecooler')}"
    };

    roomMessages = {
    bathroom : "${g.message(code: 'customer.rooms.bathroom')}",
    bedroom : "${g.message(code: 'customer.rooms.bedroom')}",
    diningroom : "${g.message(code: 'customer.rooms.diningroom')}",
    garage : "${g.message(code: 'customer.rooms.garage')}",
    kitchen : "${g.message(code: 'customer.rooms.kitchen')}",
    livingroom : "${g.message(code: 'customer.rooms.livingroom')}",
    security : "${g.message(code: 'customer.rooms.security')}",
    pool : "${g.message(code: 'customer.rooms.pool')}",
    utilityroom : "${g.message(code: 'customer.rooms.utilityroom')}"
    };

    var titleIndexConsumptionMessage = "${g.message(code: 'customer.index.calendar.consumption.title')}";
    var titleIndexPriceMessage = "${g.message(code: 'customer.index.calendar.price.title')}";

    var titleAppliancesConsumptionMessage  = "${g.message(code: 'customer.appliances.consumption.all.title')}";
    var titleAppliancesPriceMessage  = "${g.message(code: 'customer.appliances.price.all.title')}";
    var titleRoomsConsumptionMessage = "${g.message(code: 'customer.rooms.consumption.all.title')}";
    var titleRoomsPriceMessage  = "${g.message(code: 'customer.appliances.price.all.title')}";

    var tariffProfileMessage = "${g.message(code: 'customer.profile.household.tariff')}";

    var titleComparisonAppliancesMessage = "${g.message(code: 'customer.comparison.appliances.graph.title')}";
    var titleComparisonRoomsMessage = "${g.message(code: 'customer.comparison.rooms.graph.title')}";
    var graphComparisonMyMessage = 'Consumption of my room or appliance';
    var graphComparisonAvgMessage = 'Average consumption of this room or appliance';

    var graphDateMessage = "${g.message(code: 'customer.graph.date')}" ;
    var graphConsumptionMessage = "${g.message(code: 'customer.graph.electricity')}" ;
    var graphPriceMessage = "${g.message(code: 'customer.graph.price')}";

    var resultAnnualConsumptionMessage = "${g.message(code: 'customer.calculator.calculation.annualConsumption')}" ;
    var resultPriceMessage = "${g.message(code: 'customer.calculator.calculation.price')}";

    var graphExistingApplianceMessage = "${g.message(code: 'customer.calculator.calculation.existing.appliance')}" ;
    var graphSelectedApplianceMessage = "${g.message(code: 'customer.calculator.calculation.selected.appliance')}";

    var calculationHeadingMessage = "${g.message(code: 'customer.calculator.calculation.heading')}";
    var calculationReturnMessage = "${g.message(code: 'customer.calculator.returnIn')}";
    var calculationYearMessage = "${g.message(code: 'customer.calculator.year')}";
    var differencePerYearMessage = "${g.message(code: 'customer.calculator.differencePerYear')}";
    var perYearMessage = "${g.message(code: 'customer.calculator.perYear')}";

    var noOffersFoundMessage =  "${g.message(code: 'customer.calculator.noOffersFound')}";
    var neverReturnMessage = "${g.message(code: 'customer.calculator.neverReturn')}";

    var distributorMessage = "${g.message(code: 'customer.tariff.distributor')}";
    var tariffMessage = "${g.message(code: 'customer.tariff.tariff')}";
    var highRateMessage = "${g.message(code: 'customer.tariff.highRate')}";
    var lowRateMessage = "${g.message(code: 'customer.tariff.lowRate')}";

    var graphCalculatorMessage = "${g.message(code: 'customer.calculator.visualization.title')}";
    var graphHaxisMessage = "${g.message(code: 'customer.calculator.visualization.x')}";
    var graphYaxisMessage = "${g.message(code: 'customer.calculator.visualization.y')}";
    </script>
    <g:layoutHead/>
</head>
<body>

<g:render template="/templates/navbar"/>

<g:layoutBody/>

%{--<g:render template="/templates/customer/footer"/>--}%



</body>
</html>
