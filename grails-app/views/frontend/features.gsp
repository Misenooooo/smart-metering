<!doctype html>
<html>
<head>
    <meta name="layout" content="frontend"/>
    <title><g:message code="frontend.features.title"/></title>
</head>
<body>
<section class="section-heading">
    <h1 class="main-heading text-center"><g:message code="frontend.features.title"/></h1>
    <h3 class="sub-heading text-center"><g:message code="frontend.features.subtitle"/></h3>
</section>
<section class="feature white-background">
    <div class="container">
        <div id="firstRow" class="row exchange-element-wrapper-sm">
            <div class="col-sm-6 text-first-sm text-left">
                <div class="feature-text">
                    <h3><g:message code="featuresPage.firstFeature.firstTitle"/></h3>
                    <h4><g:message code="featuresPage.firstFeature.secondTitle"/></h4>
                    <p><g:message code="featuresPage.firstFeature.description"/></p>
                </div>
            </div>
            <div class="col-sm-6 feature-image image-right image-white-border image-second-sm">
                <asset:image id="first-feature-image"  src="/features-page/1.jpg"/>
            </div>
        </div>
    </div>
</section>
<section class="feature gray-background">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 feature-image image-left image-gray-border">
                <asset:image id="second-feature-image"  src="/features-page/2.jpg"/>
            </div>
            <div class="col-sm-6 text-right">
                <div class="feature-text ">
                    <h3><g:message code="featuresPage.secondFeature.firstTitle"/></h3>
                    <h4><g:message code="featuresPage.secondFeature.secondTitle"/></h4>
                    <p><g:message code="featuresPage.secondFeature.description"/></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="feature white-background">
    <div class="container">
        <div class="row exchange-element-wrapper-sm">
            <div class="col-sm-6 text-first-sm text-left">
                <div class="feature-text">
                    <h3><g:message code="featuresPage.thirdFeature.firstTitle"/></h3>
                    <h4><g:message code="featuresPage.thirdFeature.secondTitle"/></h4>
                    <p><g:message code="featuresPage.thirdFeature.description"/></p>
                </div>
            </div>
            <div class="col-sm-6 feature-image image-right image-white-border image-second-sm">
                <asset:image id="third-feature-image" src="/features-page/3.jpg"/>
            </div>
        </div>
    </div>
</section>
<section class="feature gray-background">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 feature-image image-left image-gray-border">
                <asset:image id="fourth-feature-image" src="/features-page/4.jpg"/>
            </div>
            <div class="col-sm-6 text-right">
                <div class="feature-text">
                    <h3><g:message code="featuresPage.fourthFeature.firstTitle"/></h3>
                    <h4><g:message code="featuresPage.fourthFeature.secondTitle"/></h4>
                    <p><g:message code="featuresPage.fourthFeature.description"/></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="feature white-background">
    <div class="container">
        <div class="row exchange-element-wrapper-sm">
            <div class="col-sm-6 text-first-sm text-left">
                <div class="feature-text">
                    <h3><g:message code="featuresPage.fifthFeature.firstTitle"/></h3>
                    <h4><g:message code="featuresPage.fifthFeature.secondTitle"/></h4>
                    <p><g:message code="featuresPage.fifthFeature.description"/></p>
                </div>
            </div>
            <div class="col-sm-6 feature-image image-right image-white-border image-second-sm">
                <asset:image id="fifth-feature-image"  src="/features-page/5.jpg"/>
            </div>
        </div>
    </div>
</section>
</body>
</html>
