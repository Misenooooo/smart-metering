<!doctype html>
<html>
<head>
    <meta name="layout" content="frontend"/>
    <title><g:message code="frontend.index.title"/></title>
</head>
<body>

<section id="section1">
    <div class="container-fluid">
        <h1 class="main-heading text-center"><g:message code="frontend.index.title"/></h1>
        <h3 class="sub-heading text-center"><g:message code="frontend.index.subTitle"/></h3>
    </div>
</section>
<section id="first-feature" class="feature-row white-background">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 feature-icon align-left">
                <div class="circle">
                    <i class="icon-get-money" title="<g:message code="frontend.index.feature1.heading" />"></i>
                </div>
            </div>
            <div class="col-sm-10 feature-text align-left">
                <h3><g:message code="frontend.index.feature1.heading" /></h3>
                <p><g:message code="frontend.index.feature1.text" /></p>
            </div>
        </div>
    </div>
</section>
<section class="feature-row gray-background">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 feature-icon align-left">
                <div class="circle">
                    <i class="icon-users" title="<g:message code="frontend.index.feature2.heading" />"></i>
                </div>
            </div>
            <div class="col-sm-10 feature-text align-left">
                <h3><g:message code="frontend.index.feature2.heading" /></h3>
                <p><g:message code="frontend.index.feature2.text" /></p>
            </div>
        </div>
    </div>
</section>
<section class="feature-row white-background">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 feature-icon align-left">
                <div class="circle">
                    <i class="icon-alarm" title="<g:message code="frontend.index.feature3.heading" />"></i>
                </div>
            </div>
            <div class="col-sm-10 feature-text align-left">
                <h3><g:message code="frontend.index.feature3.heading" /></h3>
                <p><g:message code="frontend.index.feature3.text" /></p>
            </div>
        </div>
    </div>
</section>
<section class="feature-row gray-background">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 feature-icon align-left">
                <div class="circle">
                    <i class="icon-locked" title="<g:message code="frontend.index.feature4.heading" />"></i>
                </div>
            </div>
            <div class="col-sm-10 feature-text align-left">
                <h3><g:message code="frontend.index.feature4.heading" /></h3>
                <p><g:message code="frontend.index.feature4.text" /></p>
            </div>
        </div>
    </div>
</section>
</body>
</html>
