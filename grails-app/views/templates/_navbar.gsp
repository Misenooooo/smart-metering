<!-- Second navbar for sign in -->
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <g:link controller="frontend" action="homepage" class="navbar-brand" href="#"><asset:image src="frontend/navbar-logo.jpg" /></g:link>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-2">
            <ul class="nav navbar-nav navbar-right">
                <sec:ifNotLoggedIn>
                    <li><g:link controller="frontend" action="features">${message(code: 'frontend.navbar.features')}</g:link></li>
                    <bp:flagSelectorLandingPage/>
                    <li><g:link class="btn btn-default btn-outline btn-circle" url="/login">${message(code: 'frontend.navbar.login')}</g:link></li>
                </sec:ifNotLoggedIn>
                <li>
                <sec:ifLoggedIn>
                    <sec:ifAllGranted roles="ROLE_CUSTOMER">
                        <li><g:link controller="customer" action="index">${message(code: 'customer.navbar.overview')}</g:link></li>
                        <li><g:link controller="customer" action="appliances">${message(code: 'customer.navbar.appliances')}</g:link></li>
                        <li><g:link controller="customer" action="rooms">${message(code: 'customer.navbar.rooms')}</g:link></li>
                        <li><g:link controller="customer" action="comparison">${message(code: 'customer.navbar.comparison')}</g:link></li>
                        <li><g:link controller="customer" action="calculator">${message(code: 'customer.navbar.calculator')}</g:link></li>
                        <li><g:link controller="customer" action="profile">${message(code: 'customer.navbar.profile')}</g:link></li>
                    </sec:ifAllGranted>
                    <bp:flagSelectorLandingPage/>
                    <li><g:link class="btn btn-default btn-outline btn-circle" url="/logout/index">${message(code: 'frontend.navbar.logout')}</g:link></li>
                </sec:ifLoggedIn>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->
