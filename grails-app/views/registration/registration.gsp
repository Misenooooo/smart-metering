<!doctype html>
<html>
<head>
    <meta name="layout" content="frontend"/>
    <link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
    <asset:stylesheet src="registration/form.css" />
    <title><g:message code="registration.title"/></title>
</head>
<body>
<section class="registration">
    <div class="form-style-10">
        <h1><g:message code="registration.form.heading.title"/><span><g:message code="registration.form.span.title"/></span></h1>
        <g:if test="${flash.message}">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div  role="status">${flash.message}</div>
                </div>
            </div>
        </g:if>
        <g:form action="register">
            <g:if test="${email}">
                <div class="section">
                    <div class="inner-wrap">
                        <div class="alert alert-warning text-center" role="alert">${email}</div>
                    </div>
                </div>
            </g:if>
            <div class="section"><span>1</span><g:message code="registration.name.username"/>
                <div class="inner-wrap">
                    <label for="email" >E-mail*
                    <input type="email" required class="form-control" id="email" name="email" placeholder="<g:message code="registration.username.placeholder"/>" value="${fieldValue(bean:user,field:'username')}"></label>
                </div>
            </div>

            <div class="section"><span>2</span><g:message code="registration.name.title"/>
                <div class="inner-wrap">
                    <label><g:message code="registration.firstName"/>
                    <input type="text"  class="form-control" id="firstName" name="firstName" placeholder="<g:message code="registration.firstName.placeholder"/>" value="${fieldValue(bean:user,field:'firstName')}"></label>
                    <label ><g:message code="registration.lastName"/>
                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="<g:message code="registration.lastName.placeholder"/>" value="${fieldValue(bean:user,field:'lastName')}"></label>
                </div>
            </div>
            <g:hasErrors bean="${user}">
                <div class="section">
                    <div class="inner-wrap">
                        <ul class="errors" role="alert">
                            <g:eachError bean="${user}" var="error">
                                <g:if test="${error.field != "username"}">
                                    <li class="text-center" <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:if>
                            </g:eachError>
                        </ul>
                    </div>
                </div>
            </g:hasErrors>
            <div class="section"><span>3</span><g:message code="registration.password.title"/>
                <div class="inner-wrap">
                    <label class="required"><g:message code="registration.password.label"/>*
                    <input type="password" required class="form-control" id="password" name="password" placeholder="<g:message code="registration.password.placeholder"/>" value="${params.password}"></label>
                    <label class="required"><g:message code="registration.repeatPassword.label"/>*
                    <input type="password" required class="form-control" id="passwordRepeat" name="passwordRepeat" placeholder="<g:message code="registration.repeatPassword.placeholder"/>" value="${params.passwordRepeat }"></label>
                </div>
            </div>
            <div class="text-center">
            <g:submitButton name="register" class="btn btn-primary" value="${message(code: 'registration.button')}" />
            </div>
        </g:form>
    </div>
</section>
</body>
</html>
