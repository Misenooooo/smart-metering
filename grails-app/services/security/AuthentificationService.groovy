package security

import grails.transaction.Transactional
import sk.smartMetring.customer.AccommodationUnit
import sk.smartMetring.customer.CustomerAccount
import sk.smartMetring.security.User

@Transactional
class AuthentificationService {

    def springSecurityService

    def getAccomodationUnitForUser() {
        try {
            def user = springSecurityService.getCurrentUser()
            def customerAccount = CustomerAccount.findByUser(user)
            def accomodationUnits = AccommodationUnit.findAll()
            for (def unit : accomodationUnits) {
                if (unit.customerAccounts.contains(customerAccount)) {
                    return unit
                }
            }
        } catch (Exception e){
            log.error(e)
            return null
        }
        return null
    }
}
