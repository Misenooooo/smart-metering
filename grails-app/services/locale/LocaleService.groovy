package locale

import grails.transaction.Transactional
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class LocaleService {

    def grailsApplication
    def messageSource

    def listLocales() {
        def msg = ""
        grailsApplication.config.app.lang.allowed.collect {
            try {
                msg = messageSource.getMessage("all.locale.$it", null, LocaleContextHolder.locale)
            } catch(e) {
                msg = it
            }
            [prefix: it, message: msg]
        }
    }

}