package data

import grails.transaction.Transactional
import sk.smartMetring.DailyData
import sk.smartMetring.Dataport
import sk.smartMetring.customer.AccommodationUnit

import java.text.SimpleDateFormat
import java.time.Instant

@Transactional
class DataportDAOService {

    def getAllUniqueDataportIDs() {
       return Dataport.executeQuery("Select Distinct d.dataid From Dataport d")
    }

    def getAllSingleDataByDayForAccommodationUnit(AccommodationUnit accommodationUnit, def applianceName) {
        log.info("Query getAllMonthlyDataForAccommodationUnit with params unit = ${accommodationUnit} and applianceName = ${applianceName}.")
        def data = Dataport.executeQuery("Select d From DailyData d where (d.dataid = :dataport_id) and (${applianceName} is not null)", /*TODO :applianceName is not null does not work, but should*/
                [dataport_id:accommodationUnit.dataportDataId]) /*,appliance_name:applianceName*/
        log.info(data.size())
        def dataByDay = [:]
        data.each {
            String date = it.local15min.substring(0, Math.min(it.local15min.toString().length(), 10))
            def consumption = it."${applianceName}"
            dataByDay.put(date,consumption)
        }
        return dataByDay
    }

    def getAllDataForSingleDay(AccommodationUnit accommodationUnit, String date) {
        log.info("Query getAllMonthlyDataForAccommodationUnit with params unit = ${accommodationUnit} and date = ${date}.")
        def data = Dataport.executeQuery("Select d From Dataport d where d.dataid = :dataport_id and d.local15min like :date",
                [dataport_id:accommodationUnit.dataportDataId,date:date+'%'])
        return data
    }

    def getFirstRowForAccommodationUnit(AccommodationUnit accommodationUnit) {
        log.info("Query getFirstRowForAccommodationUnit with params unit = ${accommodationUnit}.")
        return  Dataport.executeQuery("Select d From Dataport d where d.dataid = :dataport_id order by d.local15min desc",
                [dataport_id:accommodationUnit.dataportDataId, max:1, offset:0])
    }

    def getPriceForApplianceByDay(AccommodationUnit accommodationUnit, def applianceName){
        log.info("Query getAllMonthlyDataForAccommodationUnit with params unit = ${accommodationUnit} and applianceName = ${applianceName}.")
        def data = DailyData.executeQuery("Select ${applianceName} From DailyData d where (d.dataid = :dataport_id) and (${applianceName} is not null)", /*TODO :applianceName is not null does not work, but should*/
                [dataport_id:accommodationUnit.dataportDataId])
        return data
    }

    def getAllSingleDataByDayNotForAccommodationUnit(AccommodationUnit accommodationUnit, def name){
        def data = DailyData.executeQuery("Select avg(${name}) From DailyData d where (d.dataid != :dataport_id) group by d.local15min",
                [dataport_id:accommodationUnit.dataportDataId])
        return data
    }

    def getComparisonDataForBranches(def accommodationUnit){
        def date = DailyData.executeQuery("Select d.local15min From DailyData d where d.dataid = :dataport_id order by d.local15min desc",
                [dataport_id:accommodationUnit.dataportDataId, max: 1, offset: 0])
        def data = DailyData.executeQuery("Select d From DailyData d where d.dataid = :dataport_id and d.local15min = :date",
                [dataport_id:accommodationUnit.dataportDataId,date:date])
        log.info(data)
        return data
    }
}
