package data

import grails.transaction.Transactional
import sk.smartMetring.Appliance
import sk.smartMetring.customer.ElectricityDistributor
import sk.smartMetring.customer.Tariff

@Transactional
class CustomerDAOService {

    def getAllAppliancesOffers(def applianceType){
        return Appliance.findAllByApplianceType(applianceType)
    }

    def getAllDistributors(){
        return ElectricityDistributor.findAll()
    }

    def getAllTariffsForDistributor(def electricityDistributorID){
        def electricityDistributor = ElectricityDistributor.findById(electricityDistributorID)
        return Tariff.findAllByElectricityDistributor(electricityDistributor)
    }

}
