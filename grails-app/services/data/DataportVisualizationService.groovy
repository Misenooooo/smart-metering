package data

import grails.transaction.Transactional
import sk.smartMetring.Appliance
import sk.smartMetring.customer.AccommodationUnit

import java.text.SimpleDateFormat
import java.time.Instant

@Transactional
class DataportVisualizationService {

    def dataportDAOService

    def getAverageForSelectedBranch(AccommodationUnit accommodationUnit, def branch){
        def data = dataportDAOService.getAllSingleDataByDayNotForAccommodationUnit(accommodationUnit,branch)

    }

    def getAllConsumptionDataByDay(AccommodationUnit accommodationUnit) {
        def data = dataportDAOService.getAllSingleDataByDayForAccommodationUnit(accommodationUnit,'used')
        log.debug(data)
        def response = []
        for(def it : data.entrySet()){
            log.debug(it.key)
            def (year,month,day) = it.key.split("-")
            response.add(["year":year,"month":getMonthInJavascriptFormat(month),"day":day,"consumption":it.value])
        }
        log.debug(response)
        return response
    }

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    def getConsumptionDataForDay(AccommodationUnit accommodationUnit, def javascriptDate) {
        Date javaDate = Date.from(Instant.parse(javascriptDate));
        def sqlDate = sdf.format(javaDate)
        def data = dataportDAOService.getAllDataForSingleDay(accommodationUnit, sqlDate)
        def response = []
        for(def it : data){
            log.debug(it)
            def (date,time) = it.local15min.split(" ")
            def (year,month,day) = date.split("-")
            def (hour,minute,second) = time.split(":")
            response.add(["year":year,"month":getMonthInJavascriptFormat(month),"day":day,"hour":hour,"minute":minute,"second":second,"consumption":it.used])
        }
        log.debug(response)
        return response
    }

    def getAllAppliances(AccommodationUnit accommodationUnit){
        def data = dataportDAOService.getFirstRowForAccommodationUnit(accommodationUnit)
        def response = []
        data[0].properties.each { prop ->
            if (!prop.value.equals("")){
                if(isAppliance(prop.key.toString())) {
                    response.add(['applianceName':prop.key]);
                }
            }
        }
        log.info(response)
        return response
    }

    def getAllRooms(AccommodationUnit accommodationUnit){
        def data = dataportDAOService.getFirstRowForAccommodationUnit(accommodationUnit)
        /* This piece of code looks checks some random row of dataport table with id = this.id
        and get all properties which has some data in row and is an appliance. */
        def response = []
        data[0].properties.each { prop ->
            if (!prop.value.equals("")){ /*No data is represented by "" in dataport table*/
                if(isRoom(prop.key.toString())) {
                    response.add(['roomName':prop.key]);
                }
            }
        }
        return response
    }

    def getAllAppliancesOrRooms(AccommodationUnit accommodationUnit){
        def data = dataportDAOService.getFirstRowForAccommodationUnit(accommodationUnit)
        /* This piece of code looks checks some random row of dataport table with id = this.id
        and get all properties which has some data in row and is an appliance. */
        def response = []
        data[0].properties.each { prop ->
            if (!prop.value.equals("")){
                if(isRoom(prop.key.toString()) || isAppliance(prop.key.toString())) {
                    response.add(['name':prop.key]);
                }
            }
        }
        return response
    }


    def getAllConsumptionForApplianceByDay(AccommodationUnit accommodationUnit, def applianceName) {
        def data = dataportDAOService.getAllSingleDataByDayForAccommodationUnit(accommodationUnit,applianceName)
        def response = []
        for(def it : data.entrySet()){
            log.info(it.key)
            def (year,month,day) = it.key.split("-")
            response.add(["year":year,"month":getMonthInJavascriptFormat(month),"day":day,"consumption":it.value])
        }
        log.info(response)
        return response
    }

    def getAllConsumptionForApplianceForDay(AccommodationUnit accommodationUnit, def applianceName, def javascriptDate) {
        Date javaDate = Date.from(Instant.parse(javascriptDate));
        def sqlDate = sdf.format(javaDate)
        def data = dataportDAOService.getAllDataForSingleDay(accommodationUnit, sqlDate)
        def response = []
        for(def it : data){
            log.info(it)
            def (date,time) = it.local15min.split(" ")
            def (year,month,day) = date.split("-")
            def (hour,minute,second) = time.split(":")
            response.add(["year":year,"month":getMonthInJavascriptFormat(month),"day":day,"hour":hour,"minute":minute,"second":second,"consumption":it."${applianceName}"])
        }
        log.info(response)
        return response
    }

    def getComparisonDataForBranches(AccommodationUnit accommodationUnit){
        def data = dataportDAOService.getComparisonDataForBranches(accommodationUnit)
        def response = []
        for(def it : data){
            it.properties.each { prop ->
                if (prop.value != null){
                    if(isRoom(prop.key.toString())) {
                        log.info(it.totalConsumption)
                        log.info(prop.value)
                        response.add(['name':prop.key, 'type':'room', 'value':it.totalConsumption]);
                    } else if (isAppliance(prop.key.toString())) {
                        response.add(['name':prop.key, 'type':'appliance', 'value':it.totalConsumption]);
                    }

                }
            }
        }
        log.info(response)
        return response
    }

    def isRoom(String key){
        switch (key){
            case ~/^bathroom\d{1,3}$/:
                return true
            case ~/^bedroom\d{1,3}$/:
                return true
            case ~/^diningroom\d{1,3}$/:
                return true
            case ~/^garage\d{1,3}$/:
                return true
            case ~/^kitchen\d{1,3}$/:
                return true
            case ~/^livingroom\d{1,3}$/:
                return true
            case ~/^pool\d{1,3}$/:
                return true
            case ~/^security\d{1,3}$/:
                return true
            case ~/^utilityroom\d{1,3}$/:
                return true
            default:
                return false
        }
    }

    def isAppliance(String key){
        switch (key){
            case ~/^air\d{1,3}$/: // Air compressor circuit
                return true
            case ~/^airwindowunit\d{1,3}$/: // Window unit air conditioner
                return true
            case ~/^aquarium\d{1,3}$/: // Aquarium circuit
                return true
           /* case ~/^car\d{1,3}$/: // Electric vehicle charger
                return true*/
            case ~/^clotheswasher\d{1,3}$/: // Stand-alone clothes washing machine
                return true
            case ~/^clotheswasher_dryg\d{1,3}$/: // Clothes washing machine and natural gas-powered dryer circuit
                return true
            case ~/^dishwasher\d{1,3}$/: // Dishwasher circuit
                return true
            case ~/^disposal\d{1,3}$/: // Kitchen sink garbage disposal
                return true
            case ~/^drye\d{1,3}$/: // Electricity-powered clothes dryer (240V circuit)
                return true
            case ~/^dryg\d{1,3}$/: // Natural gas-powered clothes dryer (120V circuit).
                return true        // The eGauge will only pick up the electricity use from the dryer’s drum rotation, not the gas heating signature.
            case ~/^freezer\d{1,3}$/: // Stand-alone freezer circuit
                return true
            case ~/^furnace\d{1,3}$/: // Second furnace and air handler
                return true
            case ~/^heater\d{1,3}$/: // Stand-alone heater circuit
                return true
            case ~/^housefan\d{1,3}$/: // Whole home fan circuit
                return true
            case ~/^icemaker\d{1,3}$/: // Stand-alone icemaker circuit
                return true
            case ~/^jacuzzi\d{1,3}$/: // Jacuzzi bathtub or hot tub
                return true
            case ~/^lights_plugs\d{1,3}$/: // General lighting and plugs circuit.
                return true                // This type of circuit includes lights, fans, and wall outlets, often from multiple rooms in the home.
            case ~/^microwave\d{1,3}$/: // Microwave circuit
                return true
            case ~/^outsidelights_plugs\d{1,3}$/: // Exterior lighting and plugs circuit
                return true
            case ~/^oven\d{1,3}$/: // Oven circuit
                return true
            case ~/^pool\d{1,3}$/: // Combination pool pump and/or pool auxiliary power
                return true
            case ~/^poollight\d{1,3}$/: // Pool lighting circuit
                return true
            case ~/^poolpump\d{1,3}$/: // Pool pump circuit
                return true
            case ~/^pump\d{1,3}$/: // Data present for any type of pump that is not a pool pump
                return true
            case ~/^range\d{1,3}$/: // Range (either a stand-alone cooktop or a cooktop and an oven) circuit
                return true
            case ~/^refrigerator\d{1,3}$/: // Refrigerator circuit
                return true
            case ~/^sprinkler\d{1,3}$/: // Sprinkler system circuit
                return true
            case ~/^waterheater\d{1,3}$/: // Electric water heater
                return true
            case ~/^winecooler\d{1,3}$/: // Wine cooler circuit
                return true
            default:
                return false
        }
    }

    def getMonthInJavascriptFormat(String month){
        switch (month) {
            case '01':
                return '0'
            case '02':
                return '1'
            case '03':
                return '2'
            case '04':
                return '3'
            case '05':
                return '4'
            case '06':
                return '5'
            case '07':
                return '6'
            case '08':
                return '7'
            case '09':
                return '8'
            case '10':
                return '9'
            case '11':
                return '10'
            case '12':
                return '11'
        }
    }

}

