import sk.smartMetring.Appliance
import sk.smartMetring.customer.AccommodationUnit
import sk.smartMetring.customer.CustomerAccount
import sk.smartMetring.customer.ElectricityDistributor
import sk.smartMetring.customer.Tariff
import sk.smartMetring.security.Role
import sk.smartMetring.security.User
import sk.smartMetring.security.UserRole

class BootStrap {

    def  dataportDAOService

    def init = { servletContext ->

        def adminRole = Role.findByAuthority(Role.ADMIN) ?: new Role(authority: Role.ADMIN)
        adminRole.save(flush:true, failOnError:true)

        def customerRole = Role.findByAuthority(Role.CUSTOMER) ?: new Role(authority: Role.CUSTOMER)
        customerRole.save(flush:true, failOnError:true)

        User user = User.findByUsername('test@test.com') ?: new User(
            username: 'test@test.com',
            password: 'test'
        ).save(failOnError: 'true', flush: 'true')
        if(!UserRole.findByRoleAndUser(customerRole,user)){
            UserRole.create(user,customerRole)
        }

        User firstUser = User.findByUsername('first@smartMetring.com') ?: new User(
                username: 'first@smartMetring.com',
                password: 'smartMetring'
        ).save(failOnError: true, flush: true)


        def zse = ElectricityDistributor.findByName('Západoslovenská energetika') ?: new ElectricityDistributor(
            name: 'Západoslovenská energetika'
        ).save(failOnError: true, flush: true)

        def tariff1 = Tariff.findAllByCode("zse_price_dd1") ?: new Tariff(
            name: "dd1",
            code: "zse_price_dd1",
            highRate: 0.0419,
            electricityDistributor: zse

        ).save(failOnError: true, flush: true)

        def tariff2 = Tariff.findAllByCode("zse_price_dd2") ?: new Tariff(
                name: "dd2",
                code: "zse_price_dd2",
                highRate: 0.0502,
                electricityDistributor: zse
        ).save(failOnError: true, flush: true)


        def tariff3 = Tariff.findAllByCode("zse_price_dd3") ?: new Tariff(
                name: "dd3",
                code: "zse_price_dd3",
                lowRate: 0.0289,
                highRate: 0.0486,
                electricityDistributor: zse
        ).save(failOnError: true, flush: true)


        def tariff4 = Tariff.findAllByCode("zse_price_dd4") ?: new Tariff(
                name: "dd4",
                code: "zse_price_dd4",
                lowRate: 0.0578,
                highRate: 0.0324,
                electricityDistributor: zse
        ).save(failOnError: true, flush: true)


        def tariff5 = Tariff.findAllByCode("zse_price_dd5") ?: new Tariff(
                name: "dd5",
                code: "zse_price_dd5",
                lowRate: 0.0358,
                highRate: 0.0613,
                electricityDistributor: zse
        ).save(failOnError: true, flush: true)

        def vse = ElectricityDistributor.findByName('Východoslovenská energetika') ?: new ElectricityDistributor(
                name: 'Východoslovenská energetika'
        ).save(failOnError: true, flush: true)

        def vse_tariff1 = Tariff.findAllByCode("vse_price_dd1") ?: new Tariff(
                name: "dd1",
                code: "vse_price_dd1",
                highRate: 0.0479,
                electricityDistributor: vse
        ).save(failOnError: true, flush: true)

        def vse_tariff2 = Tariff.findAllByCode("vse_price_dd2") ?: new Tariff(
                name: "dd2",
                code: "vse_price_dd2",
                highRate: 0.0479,
                electricityDistributor: vse
        ).save(failOnError: true, flush: true)

        def vse_tariff3 = Tariff.findAllByCode("vse_price_dd3") ?: new Tariff(
                name: "dd3",
                code: "vse_price_dd3",
                lowRate: 0.0337,
                highRate: 0.0540,
                electricityDistributor: vse
        ).save(failOnError: true, flush: true)

        def vse_tariff4 = Tariff.findAllByCode("vse_price_dd4") ?: new Tariff(
                name: "dd4",
                code: "vse_price_dd4",
                lowRate: 0.0337,
                highRate: 0.0540,
                electricityDistributor: vse
        ).save(failOnError: true, flush: true)

        def vse_tariff5 = Tariff.findAllByCode("vse_price_dd5") ?: new Tariff(
                name: "dd5",
                code: "vse_price_dd5",
                lowRate: 0.0434,
                highRate: 0.0606,
                electricityDistributor: vse
        ).save(failOnError: true, flush: true)

        log.info("Creating of customer accounts and accomodation units.")
        def customerAccount
        def accomodationUnit
        def i = 0;
        for (def dataport_id : dataportDAOService.getAllUniqueDataportIDs()-1){
            user = User.findByUsername("test"+i+"@test.com") ?: new User(
                    username: "test"+i+"@test.com",
                    password: 'test'
            ).save(failOnError: 'true', flush: 'true')
            if(!UserRole.findByRoleAndUser(customerRole,user)){
                UserRole.create(user,customerRole)
            }
            customerAccount = CustomerAccount.findByUser(user) ?: new CustomerAccount(
                    user: user
            ).save(failOnError: 'true', flush: 'true')
            accomodationUnit = AccommodationUnit.findByDataportDataId(dataport_id) ?: new AccommodationUnit(
                    dataportDataId: dataport_id,
                    tariff: tariff1,
                    hashCode: dataport_id
            ).save(failOnError: 'true', flush: 'true')
            if (!accomodationUnit?.customerAccounts?.contains(customerAccount)) {
                accomodationUnit.addToCustomerAccounts(customerAccount)
                accomodationUnit.save(failOnError: 'true', flush: 'true')
            }
            i++;
        }

        log.info("Creating of customer accounts and accomodation units is finished.")

        log.info("Creating of appliances.")
        /*30.04.2017 09:55:00*/
        Appliance appliance1 = Appliance.findByWebUrl('https://lednice.heureka.sk/candy-cfo-050-e/') ?: new Appliance(
                name: 'Candy CFO 050 E',
                applianceType: 'refrigerator',
                webUrl: 'https://lednice.heureka.sk/candy-cfo-050-e/',
                yearlyConsumption: 107,
                price: 97.4667,
                capacity: 43,
                imagePath: '/candy-cfo-050-e.jpg'
        ).save()

        Appliance appliance2 = Appliance.findByWebUrl('https://lednice.heureka.sk/beko-csa-29022/') ?: new Appliance(
                name: 'Beko CSA 29022',
                applianceType: 'refrigerator',
                webUrl: 'https://lednice.heureka.sk/beko-csa-29022/',
                yearlyConsumption: 227,
                price: 207.653,
                capacity: 175,
                imagePath: '/beko-csa-29022.jpg'
        ).save()

        Appliance appliance3 = Appliance.findByWebUrl('https://lednice.heureka.sk/gorenje-rk-62-fsy2b/') ?: new Appliance(
                name: 'Gorenje RK 62 FSY2B',
                applianceType: 'refrigerator',
                webUrl: 'https://lednice.heureka.sk/gorenje-rk-62-fsy2b/',
                yearlyConsumption: 230,
                price: 386,
                capacity: 223,
                imagePath: '/gorenje-rk-62-fsy2b.jpg'
        ).save()

        Appliance appliance4 = Appliance.findByWebUrl('https://lednice.heureka.sk/whirlpool-art-9811-a-plus-plus-sf/') ?: new Appliance(
                name: 'Whirlpool ART 9811/A++SF',
                applianceType: 'refrigerator',
                webUrl: 'https://lednice.heureka.sk/whirlpool-art-9811-a-plus-plus-sf/',
                yearlyConsumption: 247,
                price: 437.663,
                capacity: 228,
                imagePath: '/whirlpool-art-9811-a-plus-plus-sf.jpg'
        ).save()

        Appliance appliance5 = Appliance.findByWebUrl('https://lednice.heureka.sk/beko-dsa-25020/') ?: new Appliance(
                name: 'Beko DSA 25020',
                applianceType: 'refrigerator',
                webUrl: 'https://lednice.heureka.sk/beko-dsa-25020/',
                yearlyConsumption: 230,
                price: 222.267,
                capacity: 179,
                imagePath: '/beko-dsa-25020.jpg'
        ).save()

        Appliance appliance6 = Appliance.findByWebUrl('https://lednice.heureka.sk/samsung-rfg-23uers1/://lednice.heureka.sk/candy-cfo-050-e/') ?: new Appliance(
                name: 'Samsung RFG 23UERS1',
                applianceType: 'refrigerator',
                webUrl: 'https://lednice.heureka.sk/samsung-rfg-23uers1/',
                yearlyConsumption: 423,
                price: 1588,
                capacity: 396,
                imagePath: '/samsung-rfg-23uers1.jpg'
        ).save()

        Appliance appliance7 = Appliance.findByWebUrl('https://klimatizacie.heureka.sk/daikin-eco-comfort-ftxb35c/') ?: new Appliance(
                name: 'Daikin Eco Comfort FTXB35C',
                applianceType: 'air',
                webUrl: 'https://klimatizacie.heureka.sk/daikin-eco-comfort-ftxb35c/',
                yearlyConsumption: 196,
                price: 561.66,
                imagePath: '/daikin-eco-comfort-ftxb35c.jpg'
        ).save()

        Appliance appliance8 = Appliance.findByWebUrl('https://klimatizacie.heureka.sk/panasonic-kit-ue12-rke/') ?: new Appliance(
                name: 'Panasonic KIT-UE12-RKE',
                applianceType: 'air',
                webUrl: 'https://klimatizacie.heureka.sk/panasonic-kit-ue12-rke/',
                yearlyConsumption: 219,
                price: 585,
                imagePath: '/panasonic-kit-ue12-rke.jpg'
        ).save()

        Appliance appliance9 = Appliance.findByWebUrl('https://klimatizacie.heureka.sk/panasonic-kit-ue9-rke/') ?: new Appliance(
                name: 'Panasonic KIT-UE9-RKE',
                applianceType: 'air',
                webUrl: 'https://klimatizacie.heureka.sk/panasonic-kit-ue9-rke/',
                yearlyConsumption: 156,
                price: 619,
                imagePath: '/Panasonic KIT-UE9-RKE.jpg'
        ).save()

        Appliance dishwasher1 = Appliance.findByWebUrl('https://umyvacky-riadu.heureka.sk/bosch-sms-53l18/') ?: new Appliance(
                name: 'Bosch SMS 53L18',
                applianceType: 'dishwasher',
                webUrl: 'https://umyvacky-riadu.heureka.sk/bosch-sms-53l18/',
                yearlyConsumption: 	258,
                price: 323,
                imagePath: '/bosch-sms-53l18.jpg'
        ).save()

        Appliance dishwasher2 = Appliance.findByWebUrl('https://umyvacky-riadu.heureka.sk/bosch-smv-53l50/') ?: new Appliance(
                name: 'Bosch SMV 53L50',
                applianceType: 'dishwasher',
                webUrl: 'https://umyvacky-riadu.heureka.sk/bosch-smv-53l50/',
                yearlyConsumption: 258,
                price: 389,
                imagePath: '/bosch-smv-53l50.jpg'
        ).save()

        Appliance dishwasher3 = Appliance.findByWebUrl('https://umyvacky-riadu.heureka.sk/electrolux-esl-4200-lo/') ?: new Appliance(
                name: 'Electrolux ESL 4200 LO',
                applianceType: 'dishwasher',
                webUrl: 'https://umyvacky-riadu.heureka.sk/electrolux-esl-4200-lo/',
                yearlyConsumption: 250,
                price: 255.30,
                imagePath: '/electrolux-esl-4200-lo.jpg'
        ).save()

        Appliance dishwasher4 = Appliance.findByWebUrl('https://umyvacky-riadu.heureka.sk/klarstein-tk50-amazonia-45_1850w/') ?: new Appliance(
                name: 'Klarstein TK50 Amazonia 45, 1850W',
                applianceType: 'dishwasher',
                webUrl: 'https://umyvacky-riadu.heureka.sk/klarstein-tk50-amazonia-45_1850w/',
                yearlyConsumption: 222,
                price: 289.90,
                imagePath: '/klarstein-tk50-amazonia-45_1850w.jpg'
        ).save()

        Appliance dishwasher5 = Appliance.findByWebUrl('https://umyvacky-riadu.heureka.sk/bosch-smv-48m30/') ?: new Appliance(
                name: 'Bosch SMV 48M30',
                applianceType: 'dishwasher',
                webUrl: 'https://umyvacky-riadu.heureka.sk/bosch-smv-48m30/',
                yearlyConsumption: 266,
                price: 448.80,
                imagePath: '/bosch-smv-48m30.jpg'
        ).save()

        Appliance range1 = Appliance.findByWebUrl('https://odsavace-par.heureka.sk/gorenje-bhp623e10x/') ?: new Appliance(
                name: 'Gorenje BHP623E10X',
                applianceType: 'range',
                webUrl: 'https://odsavace-par.heureka.sk/gorenje-bhp623e10x/',
                yearlyConsumption: 48.2,
                price: 72,
                imagePath: '/gorenje-bhp623e10x.jpg'
        ).save()

        Appliance drye1 = Appliance.findByWebUrl('https://susicky.heureka.sk/aeg-lavatherm-75781ihcs/') ?: new Appliance(
                name: 'AEG Lavatherm 75781IHCS',
                applianceType: 'drye',
                webUrl: 'https://susicky.heureka.sk/aeg-lavatherm-75781ihcs/',
                yearlyConsumption: 235,
                price: 464,
                imagePath: '/aeg-lavatherm-75781ihcs.jpg'
        ).save()

        Appliance drye2 = Appliance.findByWebUrl('https://susicky.heureka.sk/gorenje-d-8665-n/') ?: new Appliance(
                name: 'Gorenje D 8665 N',
                applianceType: 'drye',
                webUrl: 'https://susicky.heureka.sk/gorenje-d-8665-n/',
                yearlyConsumption: 218,
                price: 477,
                imagePath: '/gorenje-d-8665-n.jpg'
        ).save()

        Appliance drye3 = Appliance.findByWebUrl('https://susicky.heureka.sk/aeg-lavatherm-97689-ih-3/') ?: new Appliance(
                name: 'AEG Lavatherm 97689 IH 3',
                applianceType: 'drye',
                webUrl: 'https://susicky.heureka.sk/aeg-lavatherm-97689-ih-3/',
                yearlyConsumption: 177,
                price: 718,
                imagePath: '/aeg-lavatherm-97689-ih-3.jpg'
        ).save()

        Appliance drye4 = Appliance.findByWebUrl('https://susicky.heureka.sk/whirlpool-hscx-70311/') ?: new Appliance(
                name: 'Whirlpool HSCX 70311',
                applianceType: 'drye',
                webUrl: 'https://susicky.heureka.sk/whirlpool-hscx-70311/',
                yearlyConsumption: 258,
                price: 331.67,
                imagePath: '/whirlpool-hscx-70311.jpg'
        ).save()

        Appliance drye5 = Appliance.findByWebUrl('https://susicky.heureka.sk/gorenje-d-7565-j/') ?: new Appliance(
                name: 'Gorenje D 7565 J',
                applianceType: 'drye',
                webUrl: 'https://susicky.heureka.sk/gorenje-d-7565-j/',
                yearlyConsumption: 199,
                price: 433,
                imagePath: '/gorenje-d-7565-j.jpg'
        ).save()

        Appliance freezer1 = Appliance.findByWebUrl('https://mraznicky.heureka.sk/whirlpool-whm-25112/') ?: new Appliance(
                name: 'Whirlpool WHM 25112',
                applianceType: 'freezer',
                webUrl: 'https://mraznicky.heureka.sk/whirlpool-whm-25112/',
                yearlyConsumption: 199,
                price: 245,
                imagePath: '/gorenje-d-7565-j.jpg'
        ).save()

        Appliance freezer2 = Appliance.findByWebUrl('https://mraznicky.heureka.sk/whirlpool-afb-601/') ?: new Appliance(
                name: 'Whirlpool AFB 601',
                applianceType: 'freezer',
                webUrl: 'https://mraznicky.heureka.sk/whirlpool-afb-601/',
                yearlyConsumption: 205,
                price: 149,
                imagePath: '/whirlpool-afb-601.jpg'
        ).save()

        Appliance freezer3 = Appliance.findByWebUrl('https://mraznicky.heureka.sk/amica-fz-208_3-aa/') ?: new Appliance(
                name: 'Amica FZ 208.3 AA',
                applianceType: 'freezer',
                webUrl: 'https://mraznicky.heureka.sk/amica-fz-208_3-aa/',
                yearlyConsumption: 164.25,
                price: 206,
                imagePath: '/amica-fz-208_3-aa.jpg'
        ).save()

        Appliance freezer4 = Appliance.findByWebUrl('https://mraznicky.heureka.sk/zanussi-zfc-14400wa/') ?: new Appliance(
                name: 'Zanussi ZFC 14400WA',
                applianceType: 'freezer',
                webUrl: 'https://mraznicky.heureka.sk/zanussi-zfc-14400wa/',
                yearlyConsumption: 189.8,
                price: 183.5,
                imagePath: '/zanussi-zfc-14400wa.jpg'
        ).save()

        Appliance freezer5 = Appliance.findByWebUrl('https://mraznicky.heureka.sk/electrolux-ec2233aow1/') ?: new Appliance(
                name: 'ELECTROLUX EC2233AOW1',
                applianceType: 'freezer',
                webUrl: 'https://mraznicky.heureka.sk/electrolux-ec2233aow1/',
                yearlyConsumption: 122,
                price: 295,
                imagePath: '/electrolux-ec2233aow1.jpg'
        ).save()


        Appliance clotheswasher1 = Appliance.findByWebUrl('https://pracky.heureka.sk/aeg-okomix-l8fec49sc/') ?: new Appliance(
                name: 'AEG ÖKOMix L8FEC49SC',
                applianceType: 'clotheswasher',
                webUrl: 'https://pracky.heureka.sk/aeg-okomix-l8fec49sc/',
                yearlyConsumption: 120,
                price: 770,
                imagePath: '/aeg-okomix-l8fec49sc.jpg'
        ).save()

        Appliance clotheswasher2 = Appliance.findByWebUrl('https://pracky.heureka.sk/zanussi-zwq-61215-wa/') ?: new Appliance(
                name: 'Zanussi ZWQ 61215 WA',
                applianceType: 'clotheswasher',
                webUrl: 'https://pracky.heureka.sk/zanussi-zwq-61215-wa/',
                yearlyConsumption: 172,
                price: 216,
                imagePath: '/zanussi-zwq-61215-wa.jpg'
        ).save()

        Appliance clotheswasher3 = Appliance.findByWebUrl('https://pracky.heureka.sk/whirlpool-awe-66710/') ?: new Appliance(
                name: 'Whirlpool AWE 66710',
                applianceType: 'clotheswasher',
                webUrl: 'https://pracky.heureka.sk/whirlpool-awe-66710/',
                yearlyConsumption: 153,
                price: 232,
                imagePath: '/whirlpool-awe-66710.jpg'
        ).save()

        log.info("Creating of appliances is finished.")


    }
    def destroy = {
    }
}
