#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script creates a table, which will have consumptions or returns error without any consqeuences if the table already exists



import psycopg2
import sys


con = None

try:
    create = "CREATE TABLE daily_data  ( id SERIAL PRIMARY KEY, version bigint NOT NULL DEFAULT 1,  data_id VARCHAR(255) NOT NULL, local_15min VARCHAR(255), used DECIMAL, air1 DECIMAL, air2 DECIMAL, air3 DECIMAL, airwindowunit1 DECIMAL, aquarium1 DECIMAL, bathroom1 DECIMAL, bathroom2 DECIMAL, bedroom1 DECIMAL, bedroom2 DECIMAL, bedroom3 DECIMAL, bedroom4 DECIMAL, bedroom5 DECIMAL, car1 DECIMAL, clotheswasher1 DECIMAL, clotheswasher_dryg1 DECIMAL, diningroom1 DECIMAL, diningroom2 DECIMAL, dishwasher1 DECIMAL, disposal1 DECIMAL, drye1 DECIMAL, dryg1 DECIMAL, freezer1 DECIMAL, furnace1 DECIMAL, furnace2 DECIMAL, garage1 DECIMAL, garage2 DECIMAL, gen DECIMAL, grid DECIMAL, heater1 DECIMAL, housefan1 DECIMAL, icemaker1 DECIMAL, jacuzzi1 DECIMAL, kitchen1 DECIMAL, kitchen2 DECIMAL, kitchenapp1 DECIMAL, kitchenapp2 DECIMAL, lights_plugs1 DECIMAL, lights_plugs2 DECIMAL, lights_plugs3 DECIMAL, lights_plugs4 DECIMAL, lights_plugs5 DECIMAL, lights_plugs6 DECIMAL, livingroom1 DECIMAL, livingroom2 DECIMAL, microwave1 DECIMAL, office1 DECIMAL, outsidelights_plugs1 DECIMAL, outsidelights_plugs2 DECIMAL, oven1 DECIMAL, oven2 DECIMAL, pool1 DECIMAL, pool2 DECIMAL, poollight1 DECIMAL, poolpump1 DECIMAL, pump1 DECIMAL, range1 DECIMAL, refrigerator1 DECIMAL, refrigerator2 DECIMAL, security1 DECIMAL, shed1 DECIMAL, sprinkler1 DECIMAL, utilityroom1 DECIMAL, venthood1 DECIMAL, waterheater1 DECIMAL, waterheater2 DECIMAL, winecooler1 DECIMAL, total_consumption DECIMAL) "
	
   

    con = psycopg2.connect(database='smart-metring', user='postgres', host='localhost' ,password='asdfg')
    cur = con.cursor()

#   run only once
    cur.execute(create)
    con.commit()

    cur.execute('SELECT DISTINCT ON (dataid) dataid FROM dataport')
    dataIds = cur.fetchall()

    columns = ["used","air1","air2","air3","airwindowunit1","aquarium1","bathroom1","bathroom2", "bedroom1","bedroom2", "bedroom3","bedroom4","bedroom5", "car1", "clotheswasher1", "clotheswasher_dryg1", "diningroom1", "diningroom2", "dishwasher1", "disposal1", "drye1", "dryg1", "freezer1", "furnace1", "furnace2", "garage1", "garage2", "gen",
"grid", "heater1", "housefan1", "icemaker1", "jacuzzi1", "kitchen1", "kitchen2", "kitchenapp1", "kitchenapp2", "lights_plugs1", "lights_plugs2", "lights_plugs3", "lights_plugs4", "lights_plugs5", "lights_plugs6", "livingroom1", "livingroom2",
"microwave1", "office1", "outsidelights_plugs1", "outsidelights_plugs2", "oven1", "oven2", "pool1", "pool2", "poollight1", "poolpump1", "pump1", "range1", "refrigerator1", "refrigerator2", "security1", "shed1", "sprinkler1", "utilityroom1", "venthood1", "waterheater1", "waterheater2", "winecooler1"]	
    for column in columns:
	    output = []
	    insert = "INSERT INTO daily_data(data_id,local_15min,"+column+",total_consumption) VALUES(%s,%s,%s,%s)"
	    for dataId in dataIds :
		    output = []
		    cur.execute("SELECT "+column+",local_15min FROM dataport where dataid=(%s) order by dataid,local_15min ASC ",dataId)
		    data = cur.fetchall()
		    total_consumption = float(0)
	 	    consumption_day = float(0)
		    consumption = float(0)
		    calculated_day = None
		    for row in data :
			if(row[0]):
				day = str(row[1].split()[0])
				if(calculated_day == day):
					previous_consumption = consumption
					consumption = (float(row[0]))
					change = ((consumption - previous_consumption) / 15)
					for i in range(15):
						previous_consumption = previous_consumption + change						
						consumption_day = consumption_day + previous_consumption * 0.01 # kw	
				elif(calculated_day is None):
					calculated_day = day	
				else:
					total_consumption = total_consumption + consumption_day
		 			output.append([calculated_day, consumption_day, total_consumption])
		        		calculated_day = day
					consumption_day = float(0)

		    for it in output:
			 cur.execute(insert,(dataId,it[0],it[1],it[2]))
	    con.commit()
except psycopg2.DatabaseError, e:
    print 'Error %s' % e
    sys.exit(1)


finally:

    if con:
        con.close()
